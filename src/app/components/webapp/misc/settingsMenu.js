import React, { Component } from 'react'
import Router from 'next/router'



export default class SettingsMenu extends Component {
    render() {
        return(
            <div className='settingsMenu'>
                <SettingsMenuItem text='General' />
                <SettingsMenuItem text='Security' />
                <SettingsMenuItem text='Upgrade' />
                <SettingsMenuItem text='Logout' action={() =>  this.props.logout()}/>
                <style jsx>{`
                    .settingsMenu {
                        position: absolute;
                        right: 0;
                        top: ${this.props.state ? '50px' : '-350px'};
                        width: 320px;
                        height: auto;
                        background-color: white;
                        z-index: -2;
                        overflow: hidden;
                        transition: all .2s;
                        box-shadow: 0px 6px 25px 1px rgba(0,0,0,0.16);
                        border-radius: 0 0 10px 10px;
                    }
                `}</style>
            </div>
        )
    }
}

const SettingsMenuItem = (props) => {
    return(
        <div className='settingsMenuItem' onClick={() =>  props.action()}>
            {props.text}
            <style jsx>{`
            .settingsMenuItem {
                height: 50px;
                width: 100%;
                display: flex;
                align-items: center;
                justify-content: center;
                font-size: 18px;
                cursor: pointer;
            }
            .settingsMenuItem:hover {
                background-color: #F1F1F5;
            }
            `}</style>
        </div>
    )
}