import React, { Component } from 'react'
import { RotateSpinLoader } from 'react-css-loaders'

import Input from './input'


//TODO: Auth dialog is currently so uninspired looking. Make it prettier.
//TODO: Get signup to work

//Dev email + pass to use is 
//ldrwarlock@gmail.com
//hunter2

export default class AuthDialog extends Component {
    constructor(props) {
        super(props)
        this.state = {
            func : this.props.func,
            email : '',
            submitting: false
        }
    }
    componentWillReceiveProps(newProps) {
        if (newProps.result === false) {
            this.setState({
                submitting: false
            })
        }
    } 
    login = (email, password) => {
        this.setState({
            submitting: true
        }, () => {
            this.props.login(email, password)
        })
        
    }
    toSignUp = () => {
        this.setState({
            func : 'SIGNUP'
        })
    }
    render() {
        return(
            <div className='auth'>
                <div className={this.state.submitting ?  'submitting' : 'authDialog'}>
                    <div className='tabs'>
                        <div className='loginTab tab' onClick={() => this.setState({func: 'LOGIN'})}>
                            Login
                        </div>
                        <div className='signUpTab tab'  onClick={() => this.setState({func: 'SIGNUP'})}>
                            Sign Up
                        </div>
                    </div>
                    {this.state.func === 'LOGIN' &&
                        <Login 
                            login={(email, password) => this.login(email, password)}
                            email={this.state.email}
                            setEmail={(email) => this.setState({email})}
                            forgot={() => {}}
                            toSignUp={() => this.toSignUp()}
                        />
                    }
                    {this.state.func === 'SIGNUP' &&
                        <SignUp 
                            email={this.state.email}
                            setEmail={(email) => this.setState({email})}
                        />
                    }
                    {this.state.submitting &&
                        <div className='submitOverlay'>
                            <RotateSpinLoader size={6} duration={1.2} color={'#C9C7D7'}/>
                        </div>
                    }
                </div>
                <div className='authOverlay'>

                </div>
                <style jsx> {`
                    .auth {
                        height: 100vh;
                        width: 100vw;
                        z-index: 20;
                        position: fixed;
                        left: 0;
                        top: 0;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }
                    
                    .authOverlay {
                        height: 100vh;
                        width: 100vw;
                        background-color: #767485;
                        opacity: .65;
                        z-index: 20;
                        position: absolute;
                    }
                    .tabs {
                        position: absolute;
                        bottom: 0;
                        width: 100%;
                        height: 50px;
                        display: flex;
                        align-items: flex-start;
                        justify-content: center;
                        border-top: 1px solid #E5E6EC;
                    }
                    .tab {
                        width: 50%;
                        height: 100%;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        cursor: pointer;
                        z-index: 32;
                        transition: all .12s;
                    }
                    .loginTab:hover {
                        background-color: ${this.state.func === 'LOGIN' ? '#8382EF' : '#C9C7D7'} !important;
                        color: ${this.state.func === 'LOGIN' ? 'white' : '#767485'} !important;
                    }
                    .loginTab {
                        background-color: ${this.state.func === 'LOGIN' ? '#413FFF' : '#F1F1F5'};
                        color: ${this.state.func === 'LOGIN' ? 'white' : '#C9C7D7'};

                    }
                    .signUpTab {
                        background-color: ${this.state.func === 'SIGNUP' ? '#413FFF' : '#F1F1F5'};
                        color: ${this.state.func === 'SIGNUP' ? 'white' : '#C9C7D7'};
    
                    }
                    .signUpTab:hover {
                        background-color: ${this.state.func === 'SIGNUP' ? '#8382EF' : '#C9C7D7'} !important;
                        color: ${this.state.func === 'SIGNUP' ? 'white' : '#767485'} !important;
                    }
                    .authDialog {
                        position: relative;
                        height: 70vh;
                        min-height: 640px;
                        width: 36vw;
                        min-width: 380px;
                        max-width: 50vh;
                        border-radius: 10px;
                        background-color: white;
                        z-index: 21;
                        overflow: hidden;
                        transition: all .2s;
                    }
                    .submitting {
                        height: 120px;
                        width: 120px;
                        border-radius: 10px;
                        background-color: white;
                        z-index: 21;
                        overflow: hidden;
                        position: relative;
                        transition: all .2s;
                    }
                    .submitOverlay {
                        height: 100%;
                        width: 100%;
                        position: absolute;
                        top: 0;
                        z-index: 40;
                        background-color: white;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }
                `}</style>
            </div>
        )
    }
}


class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: this.props.email,
            pass: '',
            page: 0
        }
    }
    handleEmail = (e) => {
        this.setState({
            email : e.target.value
        }, () => {
            this.props.setEmail(this.state.email)
        })
    }
    isEmail = (value) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
        return re.test(String(value).toLowerCase());
    }
    handlePass = (e) => {
        this.setState({
            password : e.target.value
        })
    }
    isPass = (value) => {
        if (value.length >= 6) {
            return true
        } else {
            return false
        }
    }
    next = () => {
        this.setState(prevState => ({
            page : prevState.page + 1
        }))
    }
    back = () => {
        this.setState(prevState => ({
            page : prevState.page - 1
        }))
    }
    submit = () => {
        this.props.login(this.state.email, this.state.password)
    }
    render() {
        return(
            <div className='dialog'>
                                <form className='form'>
                                    <h1>Login to Teemr.</h1>
                                    {this.state.page === 0 &&
                                        <Input 
                                        title={'Email'}
                                        type={'email'}
                                        handle={(e) => this.handleEmail(e)}
                                        value={this.state.email}
                                        placeholder={'jenn@teemr.cc'}
                                        check={(value) => this.isEmail(value)}
                                        next={() => this.next()}
                                        />
                                    }
                                    {this.state.page === 1 &&
                                        <Input 
                                        title={'Password'}
                                        type={'password'}
                                        handle={(e) => this.handlePass(e)}
                                        value={this.state.password}
                                        placeholder={'••••••••'}
                                        check={(value) => this.isPass(value)}
                                        back={() => this.back()}
                                        submit={() => this.submit()}
                                        submitText={'Login'}
                                        />
                                    }
                                    <div className='optionButtons'>
                                        <div className='forgot'>
                                            Forgot Password
                                        </div>
                                    </div>
                                    
                                </form>
                                <style jsx>{`
                                h1 {
                                    text-align: center;
                                    font-size: 32px;
                                    font-weight: 500;
                                    position: absolute;
                                    top: 80px;
                                    width: 100%;
                                }
                                label {
                                    font-size: 18px;
                                    margin-bottom: 10px;
                                }
                                t {
                                    font-size: 32px;
                                    font-weight: 600;
                                    color: #3E3CF6;
                                }
                                .dialog {
                                    display: flex;
                                    flex-direction: column;
                                    align-items: center;
                                    justify-content: center;
                                    width: 100%;
                                    height: 100%;
                                    padding: 50px;
                                    box-sizing: border-box;
                                    z-index: 2;
                                    position: relative;
                                }
                                .form {
                                    display: flex;
                                    flex-direction: column;
                                    align-items: center;
                                    justify-content: center;
                                }
                                .input {
                                    display: flex;
                                    flex-direction: column;
                                    align-items: center;
                                    justify-content: center;
                                    text-align: center; 
                                }
                                button {
                                    width: 100%;
                                    height: 50px;
                                    border-radius: 25px;
                                    border: none;
                                    background-color: #3E3CF6;
                                    color: white;
                                    font-size: 20px;
                                    font-family: 'objektiv-mk2', sans-serif;
                                    box-shadow: 0px 8px 26px 1px rgba(62, 60, 246, 0.22);
                                    transition: all .1s;
                                    cursor: pointer;
                                    outline: none;
                                    margin-top: 20px;
                                }
                                button:hover {
                                    box-shadow: 0px 4px 8px 1px rgba(62, 60, 246, 0.28);
                                    
                                }
                                button:active {
                                    box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.26);
                                    transform: scale(.990);
                                }
                                .optionButtons {
                                    position: absolute;
                                    bottom: 160px;
                                    width: 100%;
                                    display: flex;
                                    align-items: center;
                                    justify-content: center;
                                    flex-direction: column;
                                }
                                .signup {
                                    margin-top: 20px;
                                    color: #3E3CF6;
                                    text-decoration: none;
                                    cursor: pointer;
                                    transition: all .1s;
                                }
                                .signup a {
                                    color: #3E3CF6;
                                    text-decoration: none;
                                    cursor: pointer;
                                    transition: all .1s;
                                }
                                .forgot {
                                    margin-top: 8px;
                                    color: #3E3CF6;
                                    text-decoration: none;
                                    cursor: pointer;
                                    transition: all .1s;
                                }
                                .forgot {
                                    color: #3E3CF6;
                                    text-decoration: none;
                                    cursor: pointer;
                                    transition: all .1s;
                                }
                                .forgot:hover {
                                    opacity: .6;
                                }
                                .signup:hover {
                                    opacity: .6;
                                }
                                input {
                                    width: 300px;
                                    background-color: #F1F1F5;
                                    height: 45px;
                                    border: none;
                                    border-radius: 8px;
                                    font-family: 'objektiv-mk2', sans-serif;
                                    font-size: 18px;
                                    overlay: none;
                                    outline: none;
                                    padding: 0 10px;
                                    box-sizing: border-box;
                                    margin-bottom: 10px;
                                    border: 1px solid #E5E6EC;
                                    transition: all .2s;
                                }
                                input:focus {
                                    border: 2px solid rgba(62, 60, 246, 0.8);
                                }
                                `}</style>
                            </div>
        )
    }
}

class SignUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email : this.props.email,
            name : '',
            phone: '',
            password : '',
            page: 0,
            showPass: false
        }
    }
    handleEmail = (e) => {
        this.setState({
            email : e.target.value
        }, () => {
            this.props.setEmail(this.state.email)
        })
    }
    handleName = (e) => {
        this.setState({
            name : e.target.value
        })
    }
    handlePhone = (e) => {
        this.setState({
            phone : e.target.value
        })
    }
    handlePassword = (e) => {
        this.setState({
            password : e.target.value
        })
    }

    submit = () => {
        if (!this.state.email || !this.state.name || !this.state.phone || !this.state.password || !this.state.password2) {
            return alert(`Please fill out everything`)
        } else if (this.state.password !== this.state.password2) {
            alert(`Passwords don't match, try again`)
            return this.setState({
                password: null,
                password2: null
            })
        } else {
            firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((user) => {
                alert('done');
                db.collection("users").add({
                    name: this.state.name,
                    email: this.state.email,
                    uid: user.uid,
                    phone: this.state.phone
                })
                .then(function(docRef) {
                    alert('db saved')
                    console.log("Document written with ID: ", docRef.id);
                })
                .catch(function(error) {
                    alert('db not saved')
                    console.error("Error adding document: ", error);
                });
                
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
              });              
        }
    }
    isEmail = (value) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
        return re.test(String(value).toLowerCase());
    }
    isPass = (value) => {
        if (value.length >= 6) {
            return true
        } else {
            return false
        }
    }
    showPass = (e) => {
        this.setState({
            showPass : e.target.checked
        })
    }
    isName = (value) => {
        if (value.length >= 2) {
            return true
        } else {
            return false
        }
    }
    isPhone = (value) => {
        if (value.length >= 6) {
            return truex
        } else {
            return false
        }
    }
    next = () => {
        this.setState(prevState => ({
            page : ++prevState.page
        }))
    }
    back = () => {
        this.setState(prevState => ({
            page : --prevState.page
        }))
    }
    render() {
        return(
            <div className='dialog'>
                <form className='form'>
                    <h1>Create a free Teemr account now.</h1>
                    {this.state.page === 0 &&<Input 
                        title={'Email'}
                        type={'email'}
                        handle={(e) => this.handleEmail(e)}
                        value={this.state.email}
                        placeholder={'jenn@teemr.cc'}
                        check={(value) => this.isEmail(value)}
                        next={() => this.next()}
                    />}
                    {this.state.page === 1 &&<Input 
                        title={'Password'}
                        type={this.state.showPass ? 'text' : 'password'}
                        handle={(e) => this.handlePassword(e)}
                        value={this.state.password}
                        placeholder={'••••••••'}
                        check={(value) => this.isPass(value)}
                        next={() => this.next()}
                        back={() => this.back()}
                        showPass={(e) => this.showPass(e)}
                        showPassVal={this.state.showPass}
                    />}
                    {this.state.page === 2 &&<Input 
                        title={'Full Name'}
                        type={'text'}
                        handle={(e) => this.handleName(e)}
                        value={this.state.name}
                        placeholder={'Jenn Mueng'}
                        check={(value) => this.isName(value)}
                        next={() => this.next()}
                        back={() => this.back()}
                    />}
                    {this.state.page === 3 &&<Input 
                        title={'Phone Number'}
                        type={'text'}
                        handle={(e) => this.handlePhone(e)}
                        value={this.state.phone}
                        placeholder={'(+66)0863937528'}
                        check={(value) => this.isPhone(value)}
                        next={() => this.next()}
                        back={() => this.back()}
                    />}
                    {this.state.page === 4 &&
                        <div>
                            <h3>
                                Is this correct?
                            </h3>
                            <p>
                                Email: {this.state.email}
                            </p>
                            <p>
                                Name: {this.state.name}
                            </p>
                            <p>
                                Phone: {this.state.phone}
                            </p>
                        </div>
                    }
                </form>
                <style jsx>{`
                h1 {
                    text-align: center;
                    font-size: 32px;
                    font-weight: 500;
                    position: absolute;
                    top: 80px;
                    width: 100%;
                }
                label {
                    font-size: 18px;
                    margin-bottom: 10px;
                }
                t {
                    font-size: 32px;
                    font-weight: 600;
                    color: #3E3CF6;
                }
                .dialog {
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    justify-content: center;
                    width: 100%;
                    height: 100%;
                    padding: 50px;
                    box-sizing: border-box;
                    z-index: 2;
                    position: relative;
                }
                .form {
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    justify-content: center;
                }
                .input {
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    justify-content: center;
                    text-align: center; 
                }
                button {
                    width: 100%;
                    height: 50px;
                    border-radius: 25px;
                    border: none;
                    background-color: #3E3CF6;
                    color: white;
                    font-size: 20px;
                    font-family: 'objektiv-mk2', sans-serif;
                    box-shadow: 0px 8px 26px 1px rgba(62, 60, 246, 0.22);
                    transition: all .1s;
                    cursor: pointer;
                    outline: none;
                    margin-top: 20px;
                }
                button:hover {
                    box-shadow: 0px 4px 8px 1px rgba(62, 60, 246, 0.28);
                    
                }
                button:active {
                    box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.26);
                    transform: scale(.990);
                }
                .optionButtons {
                    position: absolute;
                    bottom: 160px;
                    width: 100%;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    flex-direction: column;
                }
                .signup {
                    margin-top: 20px;
                    color: #3E3CF6;
                    text-decoration: none;
                    cursor: pointer;
                    transition: all .1s;
                }
                .signup a {
                    color: #3E3CF6;
                    text-decoration: none;
                    cursor: pointer;
                    transition: all .1s;
                }
                .forgot {
                    margin-top: 8px;
                    color: #3E3CF6;
                    text-decoration: none;
                    cursor: pointer;
                    transition: all .1s;
                }
                .forgot {
                    color: #3E3CF6;
                    text-decoration: none;
                    cursor: pointer;
                    transition: all .1s;
                }
                .forgot:hover {
                    opacity: .6;
                }
                .signup:hover {
                    opacity: .6;
                }
                input {
                    width: 300px;
                    background-color: #F1F1F5;
                    height: 45px;
                    border: none;
                    border-radius: 8px;
                    font-family: 'objektiv-mk2', sans-serif;
                    font-size: 18px;
                    overlay: none;
                    outline: none;
                    padding: 0 10px;
                    box-sizing: border-box;
                    margin-bottom: 10px;
                    border: 1px solid #E5E6EC;
                    transition: all .2s;
                }
                input:focus {
                    border: 2px solid rgba(62, 60, 246, 0.8);
                }
                `}</style>
            </div>
        )
    }
}

