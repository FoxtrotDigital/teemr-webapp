import React, { Component } from 'react'
import Head from './head'
import { Settings, Bell, BellOff } from 'react-feather'
import Logo from './teemr-logo-written.svg'
import SettingsMenu from './misc/settingsMenu'

export default class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            settingsMenu : false
        }
    }
    toggleSettingsMenu = () => {
        this.setState(prevState => ({
            settingsMenu : !prevState.settingsMenu
        }))
    }
    render() {
        return(
            <div className='header'>
                <Head />
                <Logo />
                <div className='buttonArea'>
                    <div className='profileArea'>
                        <div className='profileName'>
                            Jenn Mueng
                        </div>
                        <div className='profilePhoto'>
                            <img src='/static/temp-prof.jpg' alt="profile"/>
                        </div>
                    </div>
                    <button className='headerButton notiButton'>
                        <Bell size={20}/>
                        <span className='notiCount'>
                        3
                        </span>
                    </button>
                    <button className='headerButton settingsButton' onClick={() => this.toggleSettingsMenu()}>
                        <Settings size={20}/>
                    </button>
                    
                </div>
                <SettingsMenu state={this.state.settingsMenu} logout={() => this.props.logout()}/>
                <style jsx> {`
                    .header {
                        width: 100%;
                        height: 50px;
                        background-color: white;
                        position: fixed;
                        box-sizing: border-box;
                        z-index: 10;
                        display: flex;
                        flex-direction: row;
                        align-items: center;
                        justify-content: center;
                    }
                    .buttonArea {
                        display: flex;
                        flex-direction: row;
                        justify-content: flex-end;
                        align-items: flex-start;
                        position: absolute;
                        right: 0;
                    }
                    .headerButton {
                        height: 50px;
                        width: 50px;
                        background: none;
                        border: none;
                        border-left: 1px solid #E5E6EC;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        color: #767485;
                        transition: all .1s;
                        position: relative;
                        cursor: pointer;
                    }
                    .headerButton:hover {
                        background-color: #E5E6EC;
                    }
                    .notiCount {
                        position: absolute;
                        background-color: #DB6565;
                        width: 13px;
                        height: 13px;
                        border-radius: 6.5px;
                        color: white;
                        font-size: 9px;
                        line-height: 14px;
                        right: 11px;
                        top: 11px;
                    }
                    .profileName {
                        margin-right: 10px;
                    }
                    .profileArea {
                        min-width: 120px;
                        max-width: 400px;
                        width: auto;
                        box-sizing: content-box;
                        height: 50px;
                        display: flex;
                        flex-direction: row;
                        justify-content: space-between;
                        align-items: center;
                        padding: 0px 15px;
                        transition: all .1s;
                        cursor: pointer;
                        background-color: white;
                    }
                    .profileArea:hover {
                        background-color: #E5E6EC;
                    }
                    .profilePhoto {
                        width: 35px;
                        height: 35px;
                        border-radius: 10px;
                        background-color: blue;
                        overflow: hidden;
                        
                        
                    }
                    .profilePhoto img {
                        width: 35px;
                        height: 35px;
                    }
                `}</style>
            </div>
        )
    }
}