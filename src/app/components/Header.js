import React, { Component } from 'react'
import Link from 'next/link'

export default class Header extends Component {
    render() {
        return(
            <div className='header'>
                 {/*TODO: Put actual logo SVG here.*/}
                <div className='logoArea'>
                   
                    Teemr
                </div>
                <div className='menuArea'>
                    <Link>
                        <a>
                            Pricing
                        </a>
                    </Link>
                    <Link>
                        <a>
                            About
                        </a>
                    </Link>
                    <Link>
                        <a>
                            Contact
                        </a>
                    </Link>
                    <Link href='/webapp'>
                        <a className='useNow'>
                            Use Now
                        </a>
                    </Link>
                </div>
                <style jsx>{`
                    .header {
                        height: 75px;
                        width: 100%;
                        display: flex;
                        flex-direction: row;
                        justify-content: space-between;
                        align-items: center;
                        padding: 0 30px;
                        box-sizing: border-box;
                    }
                    .menuArea {
                        display: flex;
                        flex-direction: row;
                        justify-content: flex-end;
                        align-items: center;
                    }
                    .menuArea a {
                        margin: 0 15px;
                        text-decoration: none;
                    }
                    .useNow {
                        background-color: blue;
                        height: 42px;
                        width: 130px;
                        text-align: center;
                        line-height: 42px;
                        color: white;
                        border-radius: 26px;
                        margin-left: 10px !important;
                    }
                `}</style>
            </div>
        )
    }
}