import React, { Component } from 'react'

const DEFAULT_HEIGHT = 20;

export default class Textarea extends Component {

  constructor(props) {
    super(props);

    this.state = {
      height: DEFAULT_HEIGHT,
      value: "Don't get lost in the upside down",
    };

    this.setValue = this.setValue.bind(this);
    this.setFilledTextareaHeight = this.setFilledTextareaHeight.bind(this);
  }

  componentDidMount() {
    this.mounted = true;

    this.setFilledTextareaHeight();
  }

  setFilledTextareaHeight() {
    if (this.mounted) {
      const element = this.ghost;

      this.setState({
        height: element.clientHeight,
      });
    }
  }

  setValue(event) {
    const { value }= event.target;

    this.setState({ value });
  }

  getExpandableField() {
    const isOneLine = this.state.height <= DEFAULT_HEIGHT;
    const { height, value } = this.state;

    return (
      <div>
        <textarea
          className="textarea"
          name="textarea"
          id="textarea"
          autoFocus={true}
          defaultValue={value}
          style={{
            height,
            resize: isOneLine ? "none" : null
          }}
          onChange={this.setValue}
          onKeyUp={this.setFilledTextareaHeight}
        />
        <style jsx> {`
        .textarea {
            position: absolute;
            top: 0px;
            width: 250px;
            outline: none;
            font-size: 16px;
            min-height: 20px;
            padding: 0;
            box-shadow: none;
            display: block;
            border: 2px solid black;
            overflow: hidden;  // Removes scrollbar
            transition: height 0.2s ease;
            z-index: 3;
          }
        `}</style>
      </div>
    );
  }

  getGhostField() {
    return (
      <div
        className="textarea textarea--ghost"
        ref={(c) => this.ghost = c}
        aria-hidden="true"
      >
        {this.state.value}
        <style jsx> {`
        .textarea--ghost {
            opacity: 0.3;
            display: block;
            font-size: 16px;
            white-space: pre-wrap;
            word-wrap: break-word;  
            //  Uncomment below to hide the ghost div... 
            //
            //  visibility: hidden;
            //position: absolute;
            //  top: 0;
          }
          `}
        </style>
      </div>
    );
  }

  render() {
    return (
      <div className="container">
        {this.getExpandableField()}
        {this.getGhostField()}
        <style jsx>{`
        .container {
            position: relative;
          }
          
          
          
          
        `}</style>
      </div>
    );
  }
}