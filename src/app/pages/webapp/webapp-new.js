import React, { Component, createContext } from 'react'

//Redux
import { createStore, applyMiddleware } from 'redux'
import { Provider, connect } from 'react-redux'
import thunk from 'redux-thunk'

import teemr from '../../redux/reducers'
import { projects, cards, tasks, auth, global }  from '../../redux/actionCreators'

const store = createStore(
    teemr,
    applyMiddleware(thunk)
)

//Import Components
import Head from '../../components/webapp/head'
import SideBar from '../../components/webapp/sidebar-new'
import Cards from '../../components/webapp/cards/cards'

class WebApp extends Component {
    constructor(props) {
        super(props)

        this.state = {
        }
    }
    render() {
        return(
            <div>
                <Head />
                <SideBar />
                <Cards 
                    cards={this.props.cards}
                    tasks={this.props.tasks}
                />
            </div>
                
            
        )
    }
}

const mapStateToProps = state => {
    return {   
        //Map all the data objects from redux.
        projects : state.projects.data,
        cards : state.cards.data,
        tasks : state.tasks.data,
        //users : state.users.data
    }
}
   
const mapDispatchToProps = dispatch => {
return {
    newProject : (userId) => {
        dispatch(projects.new(userId))
    },
    getAuthStatus: () => {
        dispatch(auth.getStatus())
    },
    listenToProjects: (userId) => {
        dispatch(projects.listen(userId))
    },
    login : (email, password) => {
        dispatch(auth.login(email, password))
    },
    logout : () => {
        dispatch(auth.logout())
    }
}
}
   
const ConnectedWebApp = connect(
    mapStateToProps,
    mapDispatchToProps
  )(WebApp)

export default () => {
    return(
        <Provider store={store}>
            <ConnectedWebApp />
        </Provider>
    )
};