import * as Actions from '../actions'

export default (db) => {
    const creators = {
        new : (uId, pId, type) => {
            return dispatch => {
                dispatch({type : Actions.GLOBAL.LOADING}) //Dispatch the loading state
                //Get reference to the tasks that the user is a member of
                let tasksCol = db.collection('tasks').where(`members.${uId}`, "==", true)
                //Get reference to tasks in this particular project
                let tasksProj = tasksCol.where('parent', '==', pId)
                //Get a snapshot to all the tasks in the project
                tasksProj.get().then((tasksSnap) => {
                    //Array with all the tasks
                    let tasks = []
                    tasksSnap.forEach((task) => {
                        tasks.push(1)
                    })
                    //Create a new task doc in the tasks collection
                    let tasksDoc = db.collection('tasks').doc()
                    let task = {
                        id: tasksDoc.id,
                        title: 'Untitled',
                        description: 'Describe this task',
                        dates: {
                            startDate: Date.now(),
                            dueDate: Date.now() + (2 * 86400000)
                        },
                        files : [], //Get from cloud storage
                        type: type, // EXPAND or SIMPLE
                        children : [], //Populate, then sort according to user filter
                        completion : 0,
                        assignedTo: {}, //User, listen to notify, update etc.
                        members: { [uId] : true },
                        parent: pId,
                        importance : 3,
                        updated: Date.now(),
                        rank : tasks.length
                    }
                    console.log(tasksDoc.id)
                    console.log(db.collection('projects').doc(pId).id)
                    tasksDoc.set(task)
                    .then(() => {
                        dispatch({type : Actions.GLOBAL.DONELOADING})
                    })
                })
            }
        },
        listen : (uId) => {
            creators.data = {}
            return dispatch => {
                dispatch({type : Actions.GLOBAL.LOADING}) //Dispatch the loading state
                //Get reference to the tasks that the user is a member of
                let tasksCol = db.collection('tasks').where(`members.${uId}`, "==", true)
                //Returns snapshot of all the projects
                tasksCol.onSnapshot((querySnap) => {
                    //Only get the changes
                    dispatch({type: Actions.GLOBAL.LOADING})   
                    for (let i = 0; i < querySnap.docChanges.length; i++) {
                        let doc = querySnap.docChanges[i].doc;
                        
                        //Get the data of each of the changes.
                        creators.data[doc.id] = doc.data()
                        console.log(creators.data)
                        //If its deleted then delete it
                        if (creators.data[doc.id] === null || creators.data[doc.id] === undefined) {
                            alert('deleted a task')
                            delete creators.data[doc.id]
                        }
                        
                        //Listen to each project's document
                        /*docRef.onSnapshot((docSnap) => {
                            dispatch({type: Actions.GLOBAL.LOADING})         
                            tasks[doc.id] = docSnap.data()
                            if (tasks[doc.id] === null || tasks[doc.id] === undefined) {
                                delete tasks[doc.id]
                            }
                            dispatch(updateTasks(tasks))         
                            dispatch({type: Actions.GLOBAL.DONELOADING})         
                        })*/
                    }
                    dispatch(creators.reload())         
                    dispatch({type: Actions.GLOBAL.DONELOADING})  
                })
            }
        },
        reorder : (updateArray) => {
            return dispatch => {
                dispatch({type : Actions.GLOBAL.LOADING})
                console.log(updateArray)
                let batch = db.batch()
                let refs = {}
                for (let i = 0; i < updateArray.length; i++) {
                    batch.update(db.collection('tasks').doc(updateArray[i].id), {rank: updateArray[i].newRank})
                }
                
                batch.commit().then(function () {
                    console.log('batch comitted.')
                });
                
            }
        },
        reload : () => {
            return {
                type: Actions.TASKS.UPDATE,
                data : creators.data,
                updateTime: Date.now()
            }
        },
        update : (taskId, newData) => {
            return dispatch => {
                dispatch({type : Actions.GLOBAL.LOADING})
                console.log(newData);
                let taskRef = db.collection('tasks').doc(taskId)
                taskRef.get().then((taskSnap) => {
                    let oldData = taskSnap.data()
        
                    //Only get the data thats changed to minimize network data usage.
                    let changedData = {}
                    for (let key in newData) {
                        if (oldData[key] !== newData[key]) {
                            changedData[key] = newData[key]
                        }
                        //TODO: Check for changes in arrays and objects as well
                    }
                    console.log(changedData)
                    taskRef.update(Object.assign({}, changedData, {
                        updated: Date.now()
                    }))
                    .then(() => {
                        dispatch({type : Actions.GLOBAL.DONELOADING})
                    })
                })
                
                
            }
        },
        delete : (taskId) => {
            return dispatch => {
                dispatch({type : Actions.GLOBAL.LOADING})
                let taskRef = db.collection('tasks').doc(taskId)
                taskRef.get().then((taskSnap) => {
                let taskData = taskSnap.data()
                    taskRef.delete()
                    .then(() => {
                        delete creators.data[taskId]
                        dispatch(creators.reload())
                        console.log(`successfully deleted task: ${taskId}`)
                    })
                    
                    
                })
                
            }
        }
    }
    return creators
}