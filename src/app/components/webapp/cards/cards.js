import React, { Component } from 'react'
import { MoreVertical } from 'react-feather'

import populator from '../misc/populator'
import * as Editable from './editable'

export default class CardsContainer extends Component {
    constructor(props) {
        super(props)
        /*The state is initialized with the cards and tasks props.
            Both of which contain the separate arrays that contain all the contents of either one.
            Then it is fed through to a populator function that populates the required info for the cards.
        */
        this.state = {
            cards : this.props.cards,
            tasks : this.props.tasks
        }
    }
    render() {
        let props = this.props
        let state = this.state
        return(
           <div>
               {
                   populator(state.cards, state.tasks, (populatedCards => {
                       return populatedCards.map(card => (
                           <Card 
                                title={card.title}
                                members={card.members}
                                tasks={card.tasks}
                           />
                       ))
                   }))
               }
           </div>
       )
    }
}

export class Card extends Component {
    constructor(props) {
        super(props)

        this.state = {
            title : this.props.title,
            members : this.props.members,
            tasks : this.props.tasks
        }
    }
    render() {
        return(
            <div>
                <CardHeader />
                <CardTasks />
                <CardFooter />
            </div>
        )
    }
}

class CardHeader extends Component {
    constructor(props) {
        super(props)

        this.state = {
            title : this.props.title,
            labelColor : this.props.labelColor
        }
    }
    render() {
        let props = this.props
        let state = this.state

        return(
            <div className='titleBox'>
                <div 
                    className='colorTab'
                    style={{'background-color' : state.labelColor}}
                />
                <div className='cardTitle'>
                    <Editable.CardTitle 
                        value={state.title}
                        ref={(title) => this.title = title}
                        change={(val) => this.titleEdit(val)}
                        update={() => this.updateTitle()}
                        onEdit={() =>  this.startEdit()}
                    />
                    <span className='cardOptionsIcon' onClick={() => this.toggleMenu()}>
                        <MoreVertical size={22}/>
                    </span>
                </div> 
                <style jsx>{`
                    .titleBox {
                        width: 100%;
                        min-height: 56px;
                        background-color: white;
                        display: flex;
                        flex-direction: column;
                        justify-content: flex-end;
                        align-items: center;
                        box-sizing: content-box;
                        padding-top: 13px;
                    }
                    .memberCount {
                        font-size: 12px;
                        color: #767485;
                        font-weight: 300;
                    }
                    .colorTab {
                        width: 100%;
                        height: 13px;
                        position: absolute;
                        top: 0;
                        transition: all .2s;
                    }
                    .cardTitle {
                        font-weight: 400;
                        font-size: 22px;
                        height: auto;
                    }
                    .cardOptionsIcon {
                        position: absolute;
                        right: 0px;
                        top: 13px;
                        color: #767485;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        height: 56px;
                        width: 30px;
                        cursor: pointer;
                        transition: all .1s;
                    }
                    .cardOptionsIcon:hover {
                        background-color: #F1F1F5;
                    }
                `}</style>   
            </div>
        )
    }
}

const CardTasks = (props) => {
    return(
        <div>
            Tasks
        </div>
    )
}

const CardFooter = (props) => {
    return(
        <div>
            Footer
        </div>
    )
}

