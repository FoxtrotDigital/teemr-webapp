import React, { Component } from 'react'
import Link from 'next/link'
import { 
    Plus, 
    ChevronRight, 
    MoreVertical, 
    Circle, 
    XCircle, 
    X, 
    FilePlus, 
    CheckCircle, 
    Edit2, 
    Save, 
    Trash, 
    Users, 
    Clock, 
    FileText, 
    Tag
} 
from 'react-feather'
import { connect, Provider } from 'react-redux'
import { 
    projects,
    tasks,
} 
from '../../../redux/actionCreators'

import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import Menu, { ColorPicker, TaskMenu } from './menu'
import Completion from './completion'
import CheckList from './checklist'
import TextArea from './textArea'
import Task from './cardTask'
import * as Editable from './editable'
import TimelineModal from './timelineModal'

import populator from '../misc/populator'
import * as populatorFunc from '../misc/populator'

const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
  
    return result;
};

const dataHandler = (props) => {
    props.localTasks

    return(
        <Cards 
            projects={result.projects}
            tasks={result.tasks}
        />
    )
}

class Cards extends Component {
    constructor(props) {
        super(props);
        console.log('cards loaded')
        this.state = {
            data : this.props.data,
            projects : this.props.projects,
            projectsUpdateTime: Date.now(),
            tasks : this.props.tasks,
            tasksUpdateTime: Date.now(),
        }
    }
    componentWillReceiveProps(newProps) {
        if (newProps.loading) {
            this.setState({
                projects : newProps.projects,
                tasks: newProps.tasks
            })
        }
    }
    onDragEnd = (result) => {
        let props = this.props
        let tasks = populatorFunc.getTasks(props.tasks, result.destination.droppableId)
        const items = reorder(
            tasks,
            result.source.index,
            result.destination.index
          )
        let sortedItems = items
        let updateArray = []
        for (let i = 0; i < items.length; i++) {
            if (items[i].rank !== i) {
                updateArray.push({id: items[i].id, newRank: i})
                sortedItems[i].rank = i
            }
        }
        this.setState({
            tasks : sortedItems,
            tasksUpdateTime : Date.now()
        })
        props.reorderTasks(updateArray)
            
    }
    render() {
        let props = this.props
        let state = this.state
        console.log(state.projects)
        let populatedProjects = populator(state.projects, state.tasks)
        let CardsMapped = populatedProjects.map((project) => 
        <Card 
            data={project} 
            key={project.id}
            deleteProject={() => props.deleteProject(project.id)}
            newTask={(type) => props.newTask(props.userData.uid, project.id, type)}
            updateTask={(taskId, data) => props.updateTask(taskId, data)}
            deleteTask={(taskId) => props.deleteTask(taskId)}
            updateTitle={(title) => props.updateTitle(project.id, title)}
            updateLabelColor={(color) => props.updateLabelColor(project.id, color)} 
        />
        )
        
          
        /*let CardsMapped = this.props.projectsData.map((project, index) => 
            <Card 
                data={project} 
                key={project.id}
                setLabelColor={(color) => this.props.setLabelColor(color, index)} 
                deleteProject={() => this.props.deleteProject(project.id)}
                newTask={() => this.props.newTask(project.id, 'SIMPLE')}
            />
        )*/
        return(
            <DragDropContext onDragEnd={this.onDragEnd}>
                <div className='projectsWrapper'>
                    <div className='projects'>
                    {CardsMapped}
                    </div>
                    <style jsx> {`
                        .projects {
                            height: calc(100vh - 50px);
                            min-width: min-content;
                            margin: 0;
                            
                            padding: 20px 0px 20px 20px;
                            box-sizing: border-box;
                            display: flex;
                            flex-direction: row;
                            align-items: flex-start;
                            justify-content: flex-start;
                            flex-wrap: nowrap;
                        }
                        .projectsWrapper {
                            overflow: scroll;
                            margin-left: 200px;
                        }
                    `}</style>
                </div>
            </DragDropContext>
        )
    }
}

class Card extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expand : false,
            expanded : 0,
            data: this.props.data,
            menu: false,
            completion: 0,
            newMenu: false,
            colorPicker: false,
            title: this.props.data.title,
            prevTitle: this.props.data.title,
            deleting: false,
            reload: false,
            dueDate: 2,
            timelineModal: false
        }
        
        console.log(props.data);
    }
    componentWillReceiveProps(nextProps) {
        this.calculateDueDate()
        if (nextProps.data.title !== this.state.title && nextProps.data.title !== this.state.prevTitle) {
            this.setState({
                title : nextProps.data.title,
                reload : true
            }, () => {
                this.setState({
                    reload : false
                })
            })
        }
    }
    //TODO: Have the expand and shrink methods check on ID's instead of a value, this way when you delete something it updates.
    expand = () => {
        this.setState(prevState => ({
            expand: true,
            expanded : ++prevState.expanded
        }))
    }
    shrink = () => {
        this.setState(prevState => ({
            expanded: --prevState.expanded
        }), () => {
            if (this.state.expanded === 0) {
                this.setState({
                    expand: false,
                })
            }
        })
    }
    calculateDueDate = () => {
        let dueDate = this.props.data.dates.dueDate
        let day = 24*60*60*1000
        let now = Date.now()
        let result = dueDate - now
        let days = Math.round(result/day)
        console.log(result)
        this.setState({
            dueDate: days
        })
    }
    toggleMenu = () => {
        this.setState(prevState => ({
            menu : !prevState.menu
        }))
    }
    startEdit = () => {
        this.setState(prevState => ({
            prevTitle: prevState.title
        }))
    }
    titleEdit = (val) => {
        this.setState({
            title: val
        });
    }
    completionCalc = () => {
        /*let completion = 0
        for (let i = 0; i < this.state.data.tasks.length; i++) {
            completion += this.state.data.tasks[i].completion
        }
        let calced = +(completion/(this.state.data.tasks.length));*/
        this.setState({
            completion : .5
        })
    }
    updateTask = (id, newData) => {
        this.props.updateTask(id, newData)
    }
    /*updateDataChecklist = (index, newCl) => {
        let data = this.state.data;
        data.checklist[index] = newCl
        console.log(data.checklist[index]);
        this.setState({
            data : data
        }, () => {
            this.completionCalc()
        })
    }*/
    componentDidMount() {
        this.completionCalc()
    }
    toggleNew = () => {
        this.setState(prevState => ({
            newMenu : !prevState.newMenu
        }))
    }
    toggleTimelineModal = () => {
        this.setState(prevState => ({
            timelineModal: !prevState.timelineModal
        }))
    }
    updateLabelColor = (color) => {
        this.props.updateLabelColor(color)
        this.toggleColorPicker()
    }
    toggleColorPicker = () => {
        this.setState(prevState => ({
            colorPicker : !prevState.colorPicker
        }))
    }
    onDelete = () => {
        this.setState({
            deleting : true
        })
    }
    updateTitle = () => {
        this.props.updateTitle(this.state.title)
        
    }
    render() {
        let props = this.props
        let state = this.state

        return(
            <div className={state.expand ? 'card cardExpand' : 'card'}>
                <div className='titleBox'>
                    <div className='colorTab' />>
                    
                    <div className='cardTitle'>
                        {!state.reload && 
                        <Editable.CardTitle 
                            value={state.title}
                            ref={(title) => this.title = title}
                            change={(val) => this.titleEdit(val)}
                            update={() => this.updateTitle()}
                            onEdit={() =>  this.startEdit()}
                        />
                        }
                        
                        <span className='cardOptionsIcon' onClick={() => this.toggleMenu()}>
                            <MoreVertical size={22}/>
                        </span>
                    </div>
                    {state.completion === 1 
                    ? <div className='dueDate'>
                        Done
                        </div>
                    :   <div className='dueDate'>
                        {state.dueDate > 0 
                        ? `Due in ${state.dueDate} days`
                        : state.dueDate === 0 
                            ? 'Due today.'
                            : `Late by ${Math.abs(state.dueDate)} days`
                    }
                        </div>
                    }
                    
                </div>
                <div className='menuArea'>
                {state.menu && state.colorPicker 
                ?  <ColorPicker setColor={(color) => this.updateLabelColor(color)} toggle={() => this.toggleMenu()}/>
                : null }
                <Menu 
                    state={state.menu}
                    data={[
                        {
                            title: 'Edit',
                            icon: <Edit2 size={18}/>,
                            action: () => this.startEdit(),
                            toggleClose : true,
                            close : () => this.toggleMenu()
                        },
                        {
                            title: 'Label Color',
                            icon: <Tag size={18}/>,
                            action: () => this.toggleColorPicker()
                        },
                        {
                            title: 'Timeline',
                            icon: <Clock size={18}/>,
                            action: () => this.toggleTimelineModal(),
                            toggleClose : true,
                            close : () => this.toggleMenu()
                        },
                        {
                            title: 'Members',
                            icon: <Users size={18}/>
                        },
                        {
                            title: 'Delete',
                            icon: <Trash size={18}/>,
                            danger : true,
                            action : () => this.props.deleteProject()
                        }
                    ]}
                    colorSet={(color) => this.setLabelColor(color)}
                />
                 </div>

                <TaskArea 
                    //Feed the data array to the TaskArea component
                    data={props.data}

                    //Connect to functions
                    newTask={(type) => props.newTask(type)}
                    updateTask={(taskId, data) => props.updateTask(taskId, data)}
                    deleteTask={(taskId) => props.deleteTask(taskId)}
                    expand={() => this.expand()}
                    shrink={() => this.shrink()}

                    //Connect to state
                    newMenu={state.newMenu}
                />

                <div className='footerBox'>
                    <Completion completion={this.state.completion} color='#DB6565'/>
                </div>
                {state.timelineModal && <TimelineModal />}
                <style jsx> {`
                    .card {
                        height: auto;
                        max-height: calc(100vh - 90px);
                        width: 325px;
                        background-color: white;
                        border-radius: 10px;
                        box-shadow: 0px 6px 22px 1px rgba(0,0,0,0.16);
                        overflow: hidden;
                        position: relative;
                        display: flex;
                        flex-direction: column;
                        transition: all .2s;
                        margin-right: 20px;
                    }
                    .cardExpand {
                        width: 450px;
                    }
                    .titleBox {
                        width: 100%;
                        min-height: 56px;
                        background-color: white;
                        display: flex;
                        flex-direction: column;
                        justify-content: flex-end;
                        align-items: center;
                        box-sizing: content-box;
                        padding-top: 13px;
                    }
                    .memberCount {
                        font-size: 12px;
                        color: #767485;
                        font-weight: 300;
                    }
                    .colorTab {
                        background-color: ${this.props.data.labelColor};
                        width: 100%;
                        height: 13px;
                        position: absolute;
                        top: 0;
                        transition: all .2s;
                    }
                    .cardTitle {
                        font-weight: 400;
                        font-size: 22px;
                        height: auto;
                    }
                    .cardOptionsIcon {
                        position: absolute;
                        right: 0px;
                        top: 13px;
                        color: #767485;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        height: 56px;
                        width: 30px;
                        cursor: pointer;
                        transition: all .1s;
                    }
                    .cardOptionsIcon:hover {
                        background-color: #F1F1F5;
                    }
                    .menuArea {
                        position: relative;
                    }
                    .footerBox {
                        width: 100%;
                        height: 20px;
                        background-color: white;
                        display: flex;
                        flex-direction: row;
                        justify-content: center;
                        align-items: center;
                    }
                    .dueDate {
                        font-size: 12px;
                        color: #767485;
                        font-weight: 300;
                    }
                    
                `}</style>
            </div>
        )
    }
}

class TaskArea extends Component {
    constructor(props) {
        super(props)

        this.state = {
            newMenu : false
        }
    }
    toggleNewMenu = () => {
        this.setState(prevState => ({
            newMenu : !prevState.newMenu
        }))
    }
    render() {
        let props = this.props
        let state = this.state

        //Map the tasks from data
        return(
                <Droppable droppableId={props.data.id} type="TASK">
                {(provided, snapshot) => (
                    <div className='taskBox'
                        ref={provided.innerRef}
                    >
                        {props.data.tasks.map((data, index) =>
                            <Draggable key={data.id} draggableId={data.id} index={index} >
                                {(provided, snapshot) => (
                                    <div 
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    style={{
                                        ...provided.draggableProps.style,
                                        'outline': 'none',
                                        'box-shadow': snapshot.isDragging && '0px 3px 12px 1px rgba(0,0,0,0.18)',
                                        'border-bottom': '1px solid #E5E6EC'
                                    }}
                                    >
                                        <Task 
                                            data={data} 
                                            key={data.id} 
                                            updateData={(newData) => {props.updateTask(data.id, newData)}} 
                                            delete={() => props.deleteTask(data.id)}
                                            expand={() => props.expand()} 
                                            shrink={() => props.shrink()}
                                        />
                                    </div>
                                )}
                            </Draggable>
                        )}
                        {provided.placeholder}
                        {!state.newMenu 
                        ?   <div className='newTask' onClick={() => this.toggleNewMenu()}>
                                <span className='newTaskIcon'>
                                    <Plus size={20}/>
                                </span>
                                <span className='newTaskText'>
                                    New
                                </span>
                            </div>
                        :   <div className='newTask' onClick={() => this.toggleNewMenu()}>
                                <X />
                            </div>
                        }
                        
                        <Menu 
                        state={state.newMenu}
                        data={[
                            {
                                title: 'Detailed',
                                icon: <FileText size={18}/>,
                                action : () => props.newTask('DETAILED')
                            },
                            {
                                title: 'Simple',
                                icon: <CheckCircle size={18}/>,
                                action : () => props.newTask('SIMPLE')
                            },
                        ]}
                    />
                    <style jsx>{`
                        .taskBox {
                            width: 100%;
                            min-height: 50px;
                            max-height: calc(100vh - 175px);
                            overflow-y: scroll;
                            overflow-x: hidden;
                            background-color: #F1F1F5;
                            border-top: 1px solid #E5E6EC;
                        }
                        .newTask {
                            width: 100%;
                            height: 50px;
                            display: flex;
                            justify-content: center;
                            align-items: center;
                            font-size: 16px;
                            color: #C9C7D7;
                            font-weight: 300;
                            transition: all .1s;
                            cursor: pointer;
                        }
                        .newTask:hover {
                            background-color: #E5E6EC;
                            color: #767485;
                        }
                        .newTaskIcon {
                            margin-bottom: -6px;
                            margin-left: -7px;
                        }
                        .newTaskText {
                            margin-left: 3px;
                        }
                    `}</style>
                    </div>
                )}
                </Droppable>
        )
    }
}



class EditMode extends Component {
    componentDidMount() {
        this.input.focus();
    }
    render() {
        return(
            <span className='inputBox'>
                <textarea 
                    className='input' 
                    onChange={(e) => this.props.change(e)} 
                    value={this.props.value} 
                    type='text' 
                    autofocus
                    maxlength={128}
                    ref={(input) => this.input = input}
                    rows={3}
                />
                <style jsx>{`
                    .inputBox {
                        width: 320px;
                    }
                    .input {
                        min-height: 25px;
                        background: none;
                        border: none;
                        color: #767485;
                        font-size: 16px;
                        font-weight: 300;
                        font-family: 'objektiv-mk2', sans-serif;
                        border-bottom: 1px solid #767485;
                        outline: none;
                        width: 100%;
                        resize: none;
                        height: auto;
                    }   
                `}</style>
            </span>
        )
    }
}

class EditModeDesc extends Component {
    componentDidMount() {
        this.input.focus();
    }
    render() {
        return(
            <span className='inputBox'>
                <textarea 
                    className='input' 
                    onChange={(e) => this.props.change(e)} 
                    value={this.props.value} 
                    type='text' 
                    autofocus
                    maxlength={512}
                    ref={(input) => this.input = input}
                    rows={5}
                />
                <style jsx>{`
                    .inputBox {
                        width: 95%;
                    }
                    .input {
                        min-height: 25px;
                        background: none;
                        border: none;
                        color: #767485;
                        font-size: 12px;
                        font-weight: 300;
                        font-family: 'objektiv-mk2', sans-serif;
                        border-bottom: 1px solid #767485;
                        outline: none;
                        width: 100%;
                        resize: none;
                        height: auto;
                    }   
                `}</style>
            </span>
        )
    }
}



class EditModeCard2 extends Component {
    componentDidMount() {
        this.input.focus();
    }
    render() {
        return(
            <span className='inputBox'>
                <textarea 
                    className='input' 
                    onChange={(e) => this.props.change(e)} 
                    value={this.props.value} 
                    type='text' 
                    autofocus
                    maxlength={128}
                    ref={(input) => this.input = input}
                    rows={2}
                />
                <style jsx>{`
                    .inputBox {
                        width: 280px;
                    }
                    .input {
                        min-height: 25px;
                        background: none;
                        border: none;
                        color: #3D3A51;
                        font-size: 22px;
                        text-align: center;
                        margin-bottom: -6px;
                        font-weight: 400;
                        font-family: 'objektiv-mk2', sans-serif;
                        border-bottom: 1px solid #767485;
                        outline: none;
                        width: 100%;
                        resize: none;
                        height: auto;
                    }   
                `}</style>
            </span>
        )
    }
}


const mapStateToProps = state => {
    return {
      userData: state.auth.userData,
      projects: state.projects.data,
      projectsUpdateTime: state.projects.updateTime,
      tasks: state.tasks.data,
      tasksUpdateTime: state.tasks.updateTime,
      loading: state.projects.loading
    }
  }
   
const mapDispatchToProps = dispatch => {
    return {
        //Project Functions
        deleteProject : (pid) => {
            dispatch(projects.delete(pid))
        },
        updateTitle : (pId, title) => {
            dispatch(projects.updateTitle(pId, title))
        },
        updateLabelColor : (pId, color) => {
            dispatch(projects.updateLabelColor(pId, color))
        },

        //Task Functions
        newTask : (uid, pid, type) => {
            dispatch(tasks.new(uid, pid, type))
        },
        updateTask : (taskId, newData) => {
            dispatch(tasks.update(taskId, newData))
        },
        deleteTask : (taskId) => {
            dispatch(tasks.delete(taskId))
        },
        reorderTasks : (updateArray) => {
            dispatch(tasks.reorder(updateArray))
        },
    }
  }  
const ConnectedCards = connect(
    mapStateToProps,
    mapDispatchToProps
)(Cards)

export default ConnectedCards;