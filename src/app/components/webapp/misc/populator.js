const rank = (a, b) => {
    if (a.rank < b.rank) {
      return -1;
    }
    if (a.rank > b.rank) {
      return 1;
    }
    return 0;
}

//Converts card object to an array
//Inputs: object containing the key value pairs of [id] : {}
//Outputs: array of the objects
export const getCards = (cards) => {
    try {
        let cardsArray = Object.values(cards)
        return cardsArray
    }
    catch (e) {return []}
    
}

//Gets the tasks associated with the inputted project id (pId)
//Inputs: array of all tasks, project id
//Returns: an array of the tasks associated with the inputted project Id
export const getTasks = (tasks, pId) => {
    let tasksArray = []
    
    for (let task in tasks) {
        if (tasks[task].parent === pId) {
        tasksArray.push(tasks[task])
        }
    }
    //Sort according to the .rank property
    tasksArray.sort(rank)
    return tasksArray
}

/*Default function, takes objects containing the key value pairs of [id] : {} for the projects and tasks and outputs a single populated array
    Inputs: unpopulated cards array, tasks array, callback function
    Returns: Callback function containing the populated projects array*/
export default (cards, tasks, callback) => {
    let cardsArray = getCards(cards)
    for (let i = 0; i < cardsArray.length; i++) {
        cardsArray[i].tasks = getTasks(tasks, cardsArray[i].id)
    }
    return callback(cardsArray)
}