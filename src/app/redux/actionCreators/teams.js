import * as Actions from '../actions'

export default (db) => {
    const creators = {
        new : (userId) => {
            return dispatch => {
                dispatch({type: Actions.GLOBAL.LOADING})
                let teamsCol = db.collection('teams')
                teamsCol.get().then((teamsSnap) => {
                    let teams = []
                    teamsSnap.forEach((team) => {
                        teams.push(1)
                    })
                    let docRef = teamsCol.doc()
                    let newTeam = {
                        id: docRef.id,
                        title: 'New Team',
                        labelColor: '#DB6565',
                        members : { [userId] : true },
                    }
                    docRef.set(newTeam)
                    .then(function() {
                        dispatch({type: Actions.GLOBAL.DONELOADING})
                        console.log("Team successfully written!");
                    })
                    .catch(function(error) {
                        console.error("Error writing document: ", error);
                        dispatch({type: Actions.GLOBAL.DONELOADING})
                        return
                    });
                })
            }
            
        },
        delete : (pId) => {
            return dispatch => {
                dispatch({type: Actions.GLOBAL.LOADING})
                    db.collection("teams").doc(pId).delete()
                    .then(() => {
                        console.log("Team successfully deleted!");
                        dispatch({type: Actions.GLOBAL.DONELOADING})
                        dispatch({type: Actions.PROJECTS.DELETE})
                        
                    }).catch(function(error) {
                        console.error("Error removing document: ", error);
                    });
                
            }
        },
        listen : (uid) => {
            let teams = {}
            let runs = 0
            return (dispatch) => {
                //Dispatch that something is being loaded
                dispatch({type : Actions.GLOBAL.LOADING})
                //Get a Query containing projects where the user is a member of and listen
                
                return db.collection("teams").where(`members.${uid}`, "==", true)
                .onSnapshot((querySnap) => {
                    //Iterate through every project
                    let updates = {}
                    for (let i = 0; i < querySnap.docChanges.length; i++) {
                        //Initialize the inital value of 0 updates
                        if (!updates[i]) {
                            updates[i] = 0
                        }
                        let doc = querySnap.docChanges[i].doc;
                        //Listen to every single doc
                        let docRef = db.collection("teams").doc(doc.id)
                        //Listen to each project's document
                        docRef.onSnapshot((docSnap) => {
                            dispatch({type: Actions.GLOBAL.LOADING})
                            if (updates[i] === 0) { //Initial 
                                //Add the doc to the projects object
                                teams[doc.id] = docSnap.data()
                            } else { //Everytime after
                                teams[doc.id] = Object.assign({}, teams[doc.id], docSnap.data())
                            }
                            //If it's undefined or null, delete it.
                            if (teams[doc.id] === null || teams[doc.id] === undefined) {
                                delete teams[doc.id]
                            }
                            //Document [i] has updated once
                            updates[i]++
                            if (runs >= 1) {
                                dispatch(creators.update(teams))
                                runs++
                                dispatch({type: Actions.GLOBAL.DONELOADING})
                            }
                        })
                    }
                    dispatch(creators.update(teams))
                    runs++            
                    dispatch({type: Actions.GLOBAL.DONELOADING})
                });
            }
        },
        update : (teamsData) => {
            return {
                type: Actions.TEAMS.UPDATE,
                data : teamsData,
                updateTime: Date.now()
            }
        },
    }
    return creators

    
}