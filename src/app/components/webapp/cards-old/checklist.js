import React, { Component } from 'react'
import Link from 'next/link'
import Menu, { ColorPicker } from './menu'
import Completion from './completion'
import { Plus, ChevronRight, MoreVertical, Circle, XCircle, X, FilePlus, CheckCircle, Edit2, Save, Trash, Users, Clock, FileText, Tag} from 'react-feather'
import { connect, Provider } from 'react-redux'
import * as Editable from './editable'

export default class CheckList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : this.props.data
        }
    }
    toggle = (index) => {
        let data = this.state.data
        data[index].checked = !data[index].checked
        this.setState({
            data : data
        }, () => {
            this.props.updateData(this.state.data);
        })
    }
    render() {
        /*let CheckListMap = this.state.data.map((item, index) => 
            <CheckListItem 
                title={item.title}
                editMode={false}
                checked={item.checked}
                toggle={() => this.toggle(index)}
                update={() => this.props.updateData(this.state.data)}
                key={index}
                child={this.props.child}
            />
        )*/
        return(
            <div className='checkListArea'>
                <style jsx>{`
                    .checkListArea {
                        width: 100%;
                        height: auto;
                        
                    }
                `}</style>
            </div>
        )
    }
}

