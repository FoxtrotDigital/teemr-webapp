import * as Actions from '../actions'

export default (db) => {
    const creators = {
        new : (userId) => {
            return dispatch => {
                dispatch({type: Actions.GLOBAL.LOADING})
                let cardCol = db.collection('cards')
                cardCol.get().then((cardsSnap) => {
                    let cards = []
                    cardsSnap.forEach((project) => {
                        cards.push(1)
                    })
                    let docRef = cardCol.doc()
                    let newCard = {
                        id: docRef.id,
                        title: 'New Card',
                        labelColor: '#DB6565',
                        members : { [userId] : true },
                        tasks: {},
                        rank: projs.length
                    }
                    docRef.set(newCard)
                    .then(function() {
                        dispatch({type: Actions.GLOBAL.DONELOADING})
                        console.log("Project successfully written!");
                    })
                    .catch(function(error) {
                        console.error("Error writing document: ", error);
                        dispatch({type: Actions.GLOBAL.DONELOADING})
                        return
                    });
                })
            }
            
        },
    }
    return creators

    
}