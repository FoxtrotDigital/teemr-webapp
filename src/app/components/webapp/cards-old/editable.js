import React, { Component } from 'react'
import { Edit2 } from 'react-feather'

export class CardTitle extends Component {
    constructor(props){
        super(props);
        this.state = {
            value : this.props.value,
            height: 35,
            focus: false
        }
    }
    componentDidMount = () => {
        this.update()
    }
    update = () => {
        this.setState({
            height : this.ghost.clientHeight
        })
    }
    change = (e) => {
        this.setState({
            value : e.target.value,
        }, () => {
            console.log(this.input.scrollHeight)
            this.props.change(this.state.value)
            this.update()
        })
    }
    onFocus = () => {
        this.setState({
            focus : true
        })
        this.props.onEdit()
    }
    onBlur = () => {
        this.setState({
            focus : false
        }, () => {
            this.props.update()
        })
    }
    render() {
        return(
            <span className='inputBox'>
                <textarea 
                    className='input global' 
                    ref={(c) => this.input = c}
                    onChange={(e) => this.change(e)} 
                    onFocus={() => this.onFocus()}
                    onBlur={() => this.onBlur()}
                    value={this.state.value} 
                    type='text' 
                    autofocus
                    autocomplete="off"
                    autocorrect="off"
                    spellcheck="false"
                    maxlength={128}
                    ref={(input) => this.input = input}
                 />
                 {this.state.focus &&
                <div className='editIcon'>
                    <Edit2 size={15} />
                </div>}
                <div className='global ghost' ref={(c) => this.ghost = c} aria-hidden>
                    {this.state.value}
                </div>
                <style jsx>{`
                    .inputBox {
                        width: 280px;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        position: relative;
                    }
                    .global {
                        background: none;
                        border: none;
                        color: #3D3A51;
                        font-size: 22px;
                        font-weight: 400;
                        font-family: 'objektiv-mk2', sans-serif;
                        outline: none;
                        width: 100%;
                        resize: none;
                        height: auto;
                        text-align: center;
                    }
                    .input {
                        height: ${this.state.height}px !important;
                        width: 280px;
                    }
                    .input:focus {
                        border-bottom: 1px solid #767485;
                    }
                    .editIcon {
                        position: absolute;
                        bottom: 0;
                        right: 0;
                        color: #3D3A51;
                    }
                    .ghost {
                        min-height: 30px;
                        width: 275px;
                        visibility: hidden;
                        position: absolute;
                        top: -100px;
                        white-space: pre-wrap;
                        word-wrap: break-word;v
                    }
                `}</style>
            </span>
        )
    }
}

export class TaskTitle extends Component {
    constructor(props){
        super(props);
        this.state = {
            value : this.props.value,
            height: 35,
            focus: false,
            disabled : this.props.editMode
        }
    }
    componentDidMount = () => {
        this.update()
    }
    componentDidUpdate = (newProps) => {
        if (newProps.editMode === false && this.state.focus === false) {
            this.input.focus()
        }
    }
    update = () => {
        this.setState({
            height : this.ghost.clientHeight
        })
    }
    change = (e) => {
        this.setState({
            value : e.target.value,
        }, () => {
            console.log(this.input.scrollHeight)
            this.props.change(this.state.value)
            this.update()
        })
    }
    onFocus = () => {
        this.setState({
            focus : true
        })
    }
    onBlur = () => {
        this.setState({
            focus : false
        }, () => {
            this.props.exitEdit()
        })
    }
    render() {
        return(
            <span className='inputBox'>
                <textarea 
                    className='input global' 
                    ref={(c) => this.input = c}
                    onChange={(e) => this.change(e)} 
                    onFocus={() => this.onFocus()}
                    onBlur={() => this.onBlur()}
                    value={this.state.value} 
                    type='text' 
                    autocomplete="off"
                    autocorrect="off"
                    spellcheck="false"
                    maxlength={128}
                    disabled={!this.props.editMode}
                    ref={(input) => this.input = input}
                 />
                 {this.state.focus &&
                <div className='editIcon'>
                    <Edit2 size={15} />
                </div>}
                <div className='global ghost' ref={(c) => this.ghost = c} aria-hidden>
                    {this.state.value}
                </div>
                <style jsx>{`
                    .inputBox {
                        width: 250px;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        position: relative;
                        cursor: pointer;
                    }
                    .global {
                        background: none;
                        border: none;
                        color: #3D3A51;
                        font-size: 18px;
                        transform: translateY(2px);
                        font-weight: 300;
                        font-family: 'objektiv-mk2', sans-serif;
                        outline: none;
                        width: 100%;
                        resize: none;
                        height: auto;
                        text-align: left;
                    }
                    .input {
                        height: ${this.state.height}px !important;
                        width: 250px;
                        cursor: ${this.props.editMode ? 'text' : 'pointer' };
                        overflow: hidden;
                    }
                    .input:focus {
                        border-bottom: 1px solid #767485;
                    }
                    .editIcon {
                        position: absolute;
                        bottom: 0;
                        right: 0;
                        color: #3D3A51;
                    }
                    .ghost {
                        min-height: 30px;
                        width: 245px;
                        visibility: hidden;
                        position: absolute;
                        top: -100px;
                        white-space: pre-wrap;
                        word-wrap: break-word;
                        
                    }
                `}</style>
            </span>
        )
    }
}

export class TaskDesc extends Component {
    constructor(props){
        super(props);
        this.state = {
            value : this.props.value,
            height: 35,
            focus: false
        }
    }
    componentDidMount = () => {
        this.update()
    }
    update = () => {
        this.setState({
            height : this.ghost.clientHeight
        })
    }
    change = (e) => {
        this.setState({
            value : e.target.value,
        }, () => {
            console.log(this.input.scrollHeight)
            this.props.change(this.state.value)
            this.update()
        })
    }
    onFocus = () => {
        this.setState({
            focus : true
        })
    }
    onBlur = () => {
        this.setState({
            focus : false
        })
    }
    render() {
        return(
            <span className='inputBox'>
                <textarea 
                    className='input global' 
                    ref={(c) => this.input = c}
                    onChange={(e) => this.change(e)} 
                    onFocus={() => this.onFocus()}
                    onBlur={() => this.onBlur()}
                    value={this.state.value} 
                    type='text'
                    spellcheck="false"
                    autofocus
                    maxlength={1028}
                    ref={(input) => this.input = input}
                 />
                 {this.state.focus ? 
                    <div className='editIcon'>
                        <Edit2 size={15} />
                    </div> : null}
                <div className='global ghost' ref={(c) => this.ghost = c} aria-hidden>
                    {this.state.value}
                </div>
                <style jsx>{`
                    .inputBox {
                        width: 390px;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        position: relative;
                    }
                    .global {
                        background: none;
                        border: none;
                        color: #767485;
                        font-size: 12px;
                        font-weight: 300;
                        font-family: 'objektiv-mk2', sans-serif;
                        outline: none;
                        width: 100%;
                        resize: none;
                        height: auto;
                        text-align: left;
                    }
                    .input {
                        height: ${this.state.height}px !important;
                        width: 390px;
                    }
                    .input:focus {
                        border-bottom: 1px solid #767485;
                    }
                    .editIcon {
                        position: absolute;
                        bottom: 0;
                        right: 0;
                        color: #3D3A51;
                    }
                    .ghost {
                        min-height: 20px;
                        width: 385px;
                        visibility: hidden;
                        position: absolute;
                        top: -100px;
                        white-space: pre-wrap;
                        word-wrap: break-word;v
                    }
                `}</style>
            </span>
        )
    }
}

export class ChecklistTitle extends Component {
    constructor(props){
        super(props);
        this.state = {
            value : this.props.value,
            height: 35,
            focus: false,
            disabled : this.props.editMode
        }
    }
    componentDidMount = () => {
        this.update()
    }
    componentDidUpdate = (newProps) => {
        if (newProps.editMode === false && this.state.focus === false) {
            this.input.focus()
        }
    }
    update = () => {
        this.setState({
            height : this.ghost.clientHeight
        })
    }
    change = (e) => {
        this.setState({
            value : e.target.value,
        }, () => {
            console.log(this.input.scrollHeight)
            this.props.change(this.state.value)
            this.update()
        })
    }
    onFocus = () => {
        this.setState({
            focus : true
        })
    }
    onBlur = () => {
        this.setState({
            focus : false
        }, () => {
            this.props.exitEdit()
        })
    }
    render() {
        return(
            <span className='inputBox'>
                <textarea 
                    className='input global' 
                    ref={(c) => this.input = c}
                    onChange={(e) => this.change(e)} 
                    onFocus={() => this.onFocus()}
                    onBlur={() => this.onBlur()}
                    value={this.state.value} 
                    type='text' 
                    autocomplete="off"
                    autocorrect="off"
                    spellcheck="false"
                    maxlength={128}
                    disabled={!this.props.editMode}
                    ref={(input) => this.input = input}
                 />
                 {this.state.focus &&
                <div className='editIcon'>
                    <Edit2 size={15} />
                </div>}
                <div className='global ghost' ref={(c) => this.ghost = c} aria-hidden>
                    {this.state.value}
                </div>
                <style jsx>{`
                    .inputBox {
                        width: ${this.props.child ? '400px' : '100%'};
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        position: relative;
                        cursor: pointer;
                    }
                    .global {
                        background: none;
                        border: none;
                        color: #767485;
                        font-size: 18px;
                        transform: translateY(2px);
                        font-weight: 300;
                        font-family: 'objektiv-mk2', sans-serif;
                        outline: none;
                        width: 100%;
                        resize: none;
                        height: auto;
                        text-align: left;
                    }
                    .input {
                        height: ${this.state.height}px !important;
                        width: ${this.props.child ? '400px' : '100%'};
                        cursor: ${this.props.editMode ? 'text' : 'pointer' };
                        overflow: hidden;
                    }
                    .input:focus {
                        border-bottom: 1px solid #767485;
                    }
                    .editIcon {
                        position: absolute;
                        bottom: 0;
                        right: 0;
                        color: #3D3A51;
                    }
                    .ghost {
                        min-height: 30px;
                        width: ${this.props.child ? '395px' : 'calc(100% - 5px)'};
                        visibility: hidden;
                        position: absolute;
                        top: -100px;
                        white-space: pre-wrap;
                        word-wrap: break-word;
                        
                    }
                `}</style>
            </span>
        )
    }
}
