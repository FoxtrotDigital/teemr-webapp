import React, { Component } from 'react'
import { Settings, Bell, FileText, MoreHorizontal, Users, Cloud } from 'react-feather';

const SideBar = (props) => {
    return(
        <div className='sideBar'>
            <ProfileArea name={props.name} />
            <MenuArea />
            <UpgradeButton />
            <style jsx>{`
                .sideBar {
                    width : 275px;
                    height : 100vh;
                    background-color : #3D3A51;
                    box-sizing : border-box;
                    padding : 10px;
                }
                
            `}</style>
        </div>
    )
    
}

SideBar.defaultProps = {
    name : 'Jenn Mueng'
}

const ProfileArea = (props) => {
    return(
<div className='profileArea'>
        <div className='profileInfo'>
            <img className='profilePhoto' src='https://scontent.fbkk5-2.fna.fbcdn.net/v/t31.0-8/18320614_1678094175541784_8545710743701577312_o.jpg?_nc_cat=0&oh=420509d733879458a572b829b70d1b50&oe=5B5AD46F' alt='Jenn'/>
            <span className='profileName'>
                {props.name}
            </span>
        </div>
        <div className='menuButtons'>
            <div className='menuButton'>
                <Bell color='white' size={20} opacity={.9}/>
            </div>
            <div className='menuButton'>
                <Settings color='white' size={20} opacity={.9}/>
            </div>
        </div>
        <style jsx>{`
        .profileArea {
            width: 100%;
            height: 50px;
            display: flex;
            flex-direction: row;
            align-items: flex-start;
            justify-content: space-between;
            box-sizing: border-box;
            padding: 10px;
        }
        .profileInfo {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: flex-start
        }
        .profilePhoto {
            width: 50px;
            height: 50px;
            background-color: green;
            border-radius: 25px;
            border: 2px solid #C9C7D7;
            box-sizing: border-box;
            margin-right: 10px;
        }
        .profileName {
            opacity: 0.95;
            font-family: 'objektiv-mk2', serif;
            font-weight: 400;
            font-size: ${props.name.length > 12 ? props.name.length > 25 ? '10px' : '12px' : '14px'};
            max-width: 105px;
            word-wrap: break-word;
            color: #FFFFFF;
            letter-spacing: -0.06px;
        }
        .menuButtons {
            height: 50px;
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }
        .menuButton {
            width: 34px;
            height: 34px;
            border-radius: 4px;
            display: flex;
            align-items: center;
            justify-content: center;
            transition: all .15s;
            cursor: pointer;
        }
        .menuButton:hover {
            background-color: rgba(255, 255, 255, 0.2);
            
        }
    `}</style>
    </div>
    )
}

const MenuArea = (props) => {
    return(
        <div className='menuArea'>
            <MenuSection 
                title='Projects'
                icon={<FileText color='white' size={15} opacity={.9}/>}
                menus={[
                    {
                        title: 'Capture All Humans'
                    },
                    {
                        title: 'Redistribute Food'
                    }
                ]}
            />
            <MenuSection 
                title='Cloud'
                icon={<Cloud color='white' size={15} opacity={.9}/>}
                menus={[
                    {
                        title: 'Personal',
                        subtext: '2.3/4GB used'
                    },
                    {
                        title: 'Foxtrot',
                        subtext: '44/275GB used'
                    },
                    {
                        title: 'Monos',
                        subtext: '71GB used'
                    }
                ]}
            />
            <MenuSection 
                title='Teams'
                icon={<Users color='white' size={15} opacity={.9}/>}
                menus={[
                    {
                        title: 'Foxtrot',
                        subtext: '5 Members'
                    },
                    {
                        title: 'Monos',
                        subtext: '3 Members'
                    }
                ]}
            />
            <style jsx>{`
                .menuArea {
                    width: 100%;
                    height: auto;
                    display: flex;
                    flex-direction: column;
                    justify-content: flex-start;
                    align-items: center;
                    margin-top: 30px;
                }
            `}</style>
        </div>
    )
}

class MenuSection extends Component {
    constructor(props) {
        super(props)

        this.state = {
            expand: false
        }
    }
    toggleExpand = () => {
        this.setState(prevState => ({
            expand : !prevState.expand,
            height: this.menuSection.clientHeight * this.props.menus.length
        }))

        console.log(this.menuSection.clientHeight)
    }
    render() {
        let props = this.props
        let state = this.state
        let menusMapped = props.menus.map((menu) => 
            <MenuItem title={menu.title} subtext={menu.subtext}/>
        )
        return(
            <div className='menuSection' ref={men => this.menuSection = men}>
                <div className='menuTitle' onClick={() => this.toggleExpand()}>
                    <span className='titleIcon'>
                    {props.icon}
                    </span>
                    <span className='titleText'>
                    {props.title}
                    </span>
                </div>
                <div className='childrenLine' />
                <div className='menuChildren'>
                    {menusMapped}
                </div>
                <div className='menuSettings'>
                    <MoreHorizontal color='white' size={20} opacity={.9}/>
                </div>
                <style jsx>{`
                    .menuSection {
                        width: 100%;
                        height: auto;
                        display: flex;
                        flex-direction: column;
                        justify-content: flex-start;
                        align-items: center;
                        border-radius: 8px;
                        padding: ${state.expand ? '0px 10px 10px 10px' : '0px 10px 0px 10px'};
                        margin-bottom: 20px;
                        box-sizing: border-box;
                        position: relative;
                        transition: all .15s;
                        cursor: pointer;
                        overflow:hidden;
                    }
                    .menuSection:hover {
                        background-color: rgba(255, 255, 255, 0.2);
                    }
                    .menuSettings {
                        position: absolute;
                        top: 0px;
                        right: 0px;
                        transition: all .15s;
                        opacity: 0;
                        width: 40px;
                        height: 30px;
                        display: flex;
                        flex-direction: row;
                        align-items: center;
                        justify-content: center;
                        border-radius: 0 8px 0 0;
                        
                    }
                    .menuSection:hover .menuSettings {
                        opacity: .7;
                    }
                    .menuSettings:hover {
                        background-color: rgba(255, 255, 255, 0.2);
                    }
                    .menuTitle {
                        height: 30px;
                        width: 100%;
                        display: flex;
                        flex-direction: row;
                        align-items: center;
                        justify-content: flex-start;
                    }
                    .titleText {
                        color: white;
                        font-size: ${state.expand ? '14px' : '18px'};
                        font-weight: 400;
                        opacity: .92;
                        margin-left: ${state.expand ? '3px' : '8px'};
                        transition: all .1s;
                    }
                    .titleIcon {
                        transform: scale(${state.expand ? '1' : '1.3' }) translateY(1px);
                        transition: all .1s;
                    }
                    .childrenLine {
                        position: absolute;
                        left: 16px;
                        top: 30px;
                        height: calc(100% - 40px);
                        width: 1px;
                        background-color: white;
                        border-radius: 1px;
                        opacity: .2;
                    }
                    .menuChildren {
                        width: 100%;
                        max-height: ${state.expand ? `${state.height*2.66666}px` : '0px'};
                        transition: max-height .15s;
                        box-sizing: border-box;
                        
                    }
                `}</style>
            </div>
        )
    }
}

const MenuItem = (props) => {
    return(
        <div className='menuItem'>
            <div className='title'>
                {props.title}
                <span className='subText'>
                    {props.subtext}
                </span>
                
            </div>
            <div className='itemSettings'>
                    <MoreHorizontal color='white' size={20} opacity={.9}/>
                </div>
            <style jsx>{`
                .menuItem {
                    width: calc(100% + 20px);
                    height: 40px;
                    box-sizing: border-box;
                    padding-left: 30px;
                    display: flex;
                    flex-direction: row;
                    align-items: center;
                    justify-content: flex-start;
                    transition: all .15s;
                    margin: 0 -10px;
                    position: relative;
                }
                .menuItem:hover {
                    background-color: rgba(255, 255, 255, 0.2);
                }
                .title {
                    color: white;
                    font-size: 18px;
                    font-weight: 300;
                    opacity: .95;
                    display: flex;
                    flex-direction: row;
                    align-items: center;
                    justify-content: flex-start;
                    
                }
                .subText {
                    color: white;
                    font-size: 9px;
                    font-weight: 200;
                    opacity: .5;
                    margin-left: 5px;
                    transform: translateY(2px);
                }
                .itemSettings {
                    position: absolute;
                    top: 0px;
                    right: 0px;
                    transition: all .15s;
                    opacity: 0;
                    width: 40px;
                    height: 40px;
                    display: flex;
                    flex-direction: row;
                    align-items: center;
                    justify-content: center;
                    
                }
                .menuItem:hover .itemSettings {
                    opacity: .7;
                }
                .itemSettings:hover {
                    background-color: rgba(255, 255, 255, 0.2);
                }
            `}</style> 
        </div>
    )
}

const UpgradeButton = (props) => {
    return(
        <button className='upgradeButton'>
            Upgrade to Premium
            <style jsx>{`
                .upgradeButton {
                    position: absolute;
                    left: 10px;
                    bottom: 10px;
                    color: rgba(255, 255, 255, 0.5);
                    font-size: 14px;
                    background:none;
                    border: none;
                    border-radius: 8px;
                    width: 255px;
                    height: 36px;
                    box-sizing: border-box;
                    font-family: 'objektiv-mk2', sans-serif;
                    transition: all .15s;
                    cursor: pointer;
                }
                .upgradeButton:hover {
                    background-color: #7866FF;
                    color: white;
                }
            `}</style>
        </button>
    )
}

export default SideBar