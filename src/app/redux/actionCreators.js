const firebase = require('firebase')
require('firebase/firestore')
import * as Actions from './actions'
import projectActions from './actionCreators/projects'
import cardActions from './actionCreators/cards'
import taskActions from './actionCreators/tasks'
import authActions from './actionCreators/auth'


//Initialize Firebase
const config = {
    apiKey: "AIzaSyCt1lxcHqG5y94HRtsuaL8_ATuF3Lf6qMU",
    authDomain: "teemrapp.firebaseapp.com",
    databaseURL: "https://teemrapp.firebaseio.com",
    projectId: "teemrapp",
    storageBucket: "teemrapp.appspot.com",
    messagingSenderId: "206461020731"
}
try {
    firebase.initializeApp(config);
   
} catch (e) {}
const db = firebase.firestore();

//Action Creators
export const projects = projectActions(db)
export const cards = cardActions(db)
export const tasks = taskActions(db)
export const auth = authActions(db, firebase, projects, tasks)