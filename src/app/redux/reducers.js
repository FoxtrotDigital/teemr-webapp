import * as Actions from './actions'
import { combineReducers } from 'redux'

const initialState = {
    data : [
        {
            title: 'Code the Frontend',
            id: '123',
            labelColor: '#DB6565',
            members : [
                {
                    name: 'John'
                },
                {
                    name: 'Steve'
                }
            ],
            dates: {
                dueDate: 123
            },

            tasks : [],
            
        }
    ],
    page: 'CARDS',
    loading: true
}
const initialTasks = {
    data: {
        123: {
            title: 'test'
        }
    }
}

const projects = (state = initialState, action) => {
    switch (action.type) {
      case Actions.PROJECTS.UPDATE:
        return Object.assign({}, state, {
            data: action.data,
            updateTime: action.updateTime
          })
      case Actions.PROJECTS.UPDATELOCAL:
        return Object.assign({}, state, {
            localData: action.data,
            localUpdateTime: action.updateTime
        })
      case Actions.PAGE_CHANGE:
        return Object.assign({}, state, {
            page: action.page
        })
      default:
        return state
    }
  }

//TODO: Switch out the project names with card
const cards = (state = initialState, action) => {
    switch (action.type) {
      case Actions.PROJECTS.UPDATE:
        return Object.assign({}, state, {
            data: action.data,
            updateTime: action.updateTime
          })
      case Actions.PROJECTS.UPDATELOCAL:
        return Object.assign({}, state, {
            localData: action.data,
            localUpdateTime: action.updateTime
        })
      case Actions.PAGE_CHANGE:
        return Object.assign({}, state, {
            page: action.page
        })
      default:
        return state
    }
  }

  const tasks = (state = initialTasks, action) => {
      switch (action.type) {
          case Actions.TASKS.UPDATE:
            return Object.assign({}, state, {
                data: action.data,
                updateTime: action.updateTime
            })
          case Actions.TASKS.UPDATELOCAL:
            return Object.assign({}, state, {
              localData: action.data,
              localUpdateTime: action.updateTime
        })
      default:
          return state
      }
  }

const auth = (state = { userData: null, loggedIn: null }, action) => {
    switch (action.type) {
        case Actions.AUTH.SETSTATUS:
            return Object.assign({}, state, {
                userData : action.userData,
                loggedIn: action.loggedIn
            })
        case Actions.AUTH.LOGINRESULT:
            return Object.assign({}, state, {
                result: action.result
            })
        default:
            return state
    }
}

const global = (state = {loading : false}, action) => {
    switch (action.type) {
      case Actions.GLOBAL.LOADING: 
        return Object.assign({}, state, {
            loading: true
        })
        case Actions.GLOBAL.DONELOADING: 
        return Object.assign({}, state, {
            loading: false
        })
        default:
            return state
    }
}
  

const teemrWebapp = combineReducers({
    projects,
    cards,
    tasks,
    auth,
    global
})

export default teemrWebapp