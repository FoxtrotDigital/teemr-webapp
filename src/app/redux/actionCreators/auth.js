import * as Actions from '../actions'

export default (db, firebase, projects, tasks) => {
    const creators = {
        getStatus : () => {
            return (dispatch) => {
                dispatch({type: Actions.AUTH.LOADING})
                return firebase.auth().onAuthStateChanged((user) => {
                        if (user) {
                            dispatch(projects.listen(user.uid))
                            dispatch(tasks.listen(user.uid))
                            dispatch(creators.setStatus(user, true))
                        } else {
                            dispatch(creators.setStatus(null, false))
                        }
                      });
                
            }
        },
        setStatus : (userData, loggedIn) => {
            return{
                type: Actions.AUTH.SETSTATUS,
                userData,
                loggedIn
            }
        },
        login : (email, password) => {
            return dispatch => {
                firebase.auth().signInWithEmailAndPassword(email, password).then(() => {
                    console.log('successfully logged in')
                }).catch(function(error) {
                    dispatch({type: Actions.AUTH.LOGINRESULT, result: false})
                    // ...
                  });
            }
        },
        logout : () => {
            return dispatch => {
                firebase.auth().signOut().then(function() {
                    console.log('signout successful')
                    dispatch({type: Actions.AUTH.LOGOUT})
                  }).catch(function(error) {
                    console.log(`signout failure : ${error.message}`)
                  });
            }
        }
    }
    return creators
}