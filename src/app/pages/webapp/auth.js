import React, { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'
import Head from '../../components/webapp/head'
import Logo from './logo.svg'
import axios from 'axios'
const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

const config = {
    apiKey: "AIzaSyCt1lxcHqG5y94HRtsuaL8_ATuF3Lf6qMU",
    authDomain: "teemrapp.firebaseapp.com",
    databaseURL: "https://teemrapp.firebaseio.com",
    projectId: "teemrapp",
    storageBucket: "teemrapp.appspot.com",
    messagingSenderId: "206461020731"
}
try {
    firebase.initializeApp(config);
    const db = firebase.firestore();
} catch(e) {
    console.log(e)
}

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        Router.push('/webapp/projects')
    } else {

    }
  });


export default class Auth extends Component {
    static async getInitialProps({ query, res }) {
        if (query.func !== 'signup' && query.func !== 'login') {
            res.writeHead(302, {
                Location: '/webapp/auth?func=login'
            })
            res.end()
            res.finished = true
            return {}
        }
        return { func: query.func}
    }
    render() {
        return(
            <div className='authPage'>
                <Head />
                    {this.props.func === 'login' && <Login />}
                    {this.props.func === 'signup' && <SignUp />}
                    <img className='splash'src="/loginSplash.jpg" alt="stock"/>
                <style jsx> {`
                    :global(body) {
                        padding: 0;
                        margin: 0;
                        background-color: #F1F1F5;
                        font-family: 'objektiv-mk2', sans-serif;
                        color: #3D3A51;
                        overflow: hidden;
                        box-sizing: border-box;
                       
                    }
                    .splash {
                        position: absolute;
                        right: 0;
                        top: 0;
                        height: 100vh;
                        z-index: 1;
                        transform: translateX(25%);
                    }
                    
                    .authPage {
                        display: flex;
                        align-items: center;
                        justify-content: flex-start;
                        height: 100vh;
                        width: 100vw;
                        text-align: center;
                    }
                    
                `}</style>
            </div>
        )
    }
}


class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            pass: ''
        }
    }
    handleEmail = (e) => {
        this.setState({
            email : e.target.value
        })
    }
    handlePass = (e) => {
        this.setState({
            password : e.target.value
        })
    }
    submit = () => {
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(() => {
            alert('done again')
        }).catch(function(error) {
            // Handle Errors here.
            alert(error.code);
            alert(error.message);
            // ...
          });
    }
    render() {
        return(
            <div className='dialog'>
                            <Logo style={{height: '80px', width: '60px', 'align-self': 'flex-start'}}/>
                                <form className='form'>
                                    <h1>Now everyone can work together, <br/> <t>in Realtime.</t></h1>
                                    <div className='input'>
                                        <label>Email </label>
                                        <input type='email' value={this.state.email} onChange={(e) => this.handleEmail(e)}/><br />
                                    </div>
                                    <div className='input'>
                                        <label>Password </label>
                                        <input type='password' value={this.state.password} onChange={(e) => this.handlePass(e)}/>
                                    </div>
                                    <button type='button' onClick={() => this.submit()}>Login</button>
                                    <div className='signup'>
                                        or <Link href='/webapp/auth?func=signup'><a>Sign Up</a></Link>
                                    </div>
                                </form>
                                <style jsx>{`
                                h1 {
                                    text-align: left;
                                    font-size: 28px;
                                    font-weight: 300;
                                }
                                label {
                                    font-size: 18px;
                                    margin-bottom: 5px;
                                }
                                t {
                                    font-size: 32px;
                                    font-weight: 600;
                                    color:#3E3CF6;
                                }
                                .dialog {
                                    display: flex;
                                    flex-direction: column;
                                    align-items: flex-start;
                                    justify-content: center;
                                    width: 50vw;
                                    min-width: 600px;
                                    background-color: white;
                                    height: 100vh;
                                    padding: 70px;
                                    box-sizing: border-box;
                                    z-index: 2;
                                }
                                .form {
                                    display: flex;
                                    flex-direction: column;
                                    align-items: flex-start;
                                    justify-content: center;
                                }
                                .input {
                                    display: flex;
                                    flex-direction: column;
                                    align-items: flex-start;
                                    justify-content: center;
                                    text-align: center;
                                    
                                }
                                button {
                                    width: 300px;
                                    height: 50px;
                                    border-radius: 25px;
                                    border: none;
                                    background-color: #3E3CF6;
                                    color: white;
                                    font-size: 20px;
                                    font-family: 'objektiv-mk2', sans-serif;
                                    box-shadow: 0px 8px 25px 1px rgba(0,0,0,0.18);
                                    transition: all .1s;
                                    cursor: pointer;
                                    outline: none;
                                    margin-top: 20px;
                                }
                                button:hover {
                                    box-shadow: 0px 4px 8px 1px rgba(0,0,0,0.22);
                                    
                                }
                                button:active {
                                    box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.26);
                                    transform: scale(.990);
                                }
                                .signup {
                                    margin-top: 20px;
                                    color: #3E3CF6;
                                    text-decoration: none;
                                    cursor: pointer;
                                    transition: all .1s;
                                }
                                .signup a {
                                    color: #3E3CF6;
                                    text-decoration: none;
                                    cursor: pointer;
                                    transition: all .1s;
                                }
                                .signup:hover {
                                    opacity: .6;
                                }
                                input {
                                    width: 300px;
                                    background-color: #F1F1F5;
                                    height: 40px;
                                    border: none;
                                    border-radius: 10px;
                                    font-family: 'objektiv-mk2', sans-serif;
                                    font-size: 18px;
                                    overlay: none;
                                    outline: none;
                                    padding: 0 10px;
                                    box-sizing: border-box;
                                    margin-bottom: 10px;
                                }
                                `}</style>
                            </div>
        )
    }
}

class SignUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email : '',
            name : '',
            phone: '',
            password : '',
            page: 0,
            showPass: false
        }
    }
    handleEmail = (e) => {
        this.setState({
            email : e.target.value
        })
    }
    handleName = (e) => {
        this.setState({
            name : e.target.value
        })
    }
    handlePhone = (e) => {
        this.setState({
            phone : e.target.value
        })
    }
    handlePassword = (e) => {
        this.setState({
            password : e.target.value
        })
    }
    handlePasswordTwo = (e) => {
        this.setState({
            password2 : e.target.value
        })
    }
    submit = () => {
        if (!this.state.email || !this.state.name || !this.state.phone || !this.state.password || !this.state.password2) {
            return alert(`Please fill out everything`)
        } else if (this.state.password !== this.state.password2) {
            alert(`Passwords don't match, try again`)
            return this.setState({
                password: null,
                password2: null
            })
        } else {
            firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((user) => {
                alert('done');
                db.collection("users").add({
                    name: this.state.name,
                    email: this.state.email,
                    uid: user.uid,
                    phone: this.state.phone
                })
                .then(function(docRef) {
                    alert('db saved')
                    console.log("Document written with ID: ", docRef.id);
                })
                .catch(function(error) {
                    alert('db not saved')
                    console.error("Error adding document: ", error);
                });
                
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
              });              
        }
    }
    isEmail = (value) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
        return re.test(String(value).toLowerCase());
    }
    isPass = (value) => {
        if (value.length >= 6) {
            return true
        } else {
            return false
        }
    }
    showPass = (e) => {
        this.setState({
            showPass : e.target.checked
        })
    }
    isName = (value) => {
        if (value.length >= 2) {
            return true
        } else {
            return false
        }
    }
    isPhone = (value) => {
        if (value.length >= 6) {
            return truex
        } else {
            return false
        }
    }
    next = () => {
        this.setState(prevState => ({
            page : ++prevState.page
        }))
    }
    back = () => {
        this.setState(prevState => ({
            page : --prevState.page
        }))
    }
    render() {
        return(
            <div className='dialog'>
                <form className='form'>
                    <h1>Now everyone can work together, <br/> <t>in Realtime.</t></h1>
                    <h2>Create a free Teemr account now.</h2>
                    {this.state.page === 0 &&<Input 
                        title={'Email'}
                        type={'email'}
                        handle={(e) => this.handleEmail(e)}
                        value={this.state.email}
                        placeholder={'jenn@teemr.cc'}
                        check={(value) => this.isEmail(value)}
                        next={() => this.next()}
                    />}
                    {this.state.page === 1 &&<Input 
                        title={'Password'}
                        type={this.state.showPass ? 'text' : 'password'}
                        handle={(e) => this.handlePassword(e)}
                        value={this.state.password}
                        placeholder={'••••••••'}
                        check={(value) => this.isPass(value)}
                        next={() => this.next()}
                        back={() => this.back()}
                        showPass={(e) => this.showPass(e)}
                        showPassVal={this.state.showPass}
                    />}
                    {this.state.page === 2 &&<Input 
                        title={'Full Name'}
                        type={'text'}
                        handle={(e) => this.handleName(e)}
                        value={this.state.name}
                        placeholder={'Jenn Mueng'}
                        check={(value) => this.isName(value)}
                        next={() => this.next()}
                        back={() => this.back()}
                    />}
                    {this.state.page === 3 &&<Input 
                        title={'Phone Number'}
                        type={'text'}
                        handle={(e) => this.handlePhone(e)}
                        value={this.state.phone}
                        placeholder={'(+66)0863937528'}
                        check={(value) => this.isPhone(value)}
                        next={() => this.next()}
                        back={() => this.back()}
                    />}
                    {this.state.page === 4 &&
                        <div>
                            <h3>
                                Is this correct?
                            </h3>
                            <p>
                                Email: {this.state.email}
                            </p>
                            <p>
                                Name: {this.state.name}
                            </p>
                            <p>
                                Phone: {this.state.phone}
                            </p>
                        </div>
                    }
                    <div className='signup'>
                        or <Link href='/webapp/auth?func=login'><a>Login</a></Link>
                    </div>
                </form>
                <style jsx>{`
                h1 {
                    text-align: left;
                    font-size: 28px;
                    font-weight: 300;
                }
                h2 {
                    text-align: left;
                    font-size: 22px;
                    font-weight: 300;
                }
                label {
                    font-size: 18px;
                    margin-bottom: 5px;
                }
                t {
                    font-size: 32px;
                    font-weight: 600;
                    color:#3E3CF6;
                }
                .dialog {
                    display: flex;
                    flex-direction: column;
                    align-items: flex-start;
                    justify-content: center;
                    width: 50vw;
                    min-width: 600px;
                    background-color: white;
                    height: 100vh;
                    padding: 70px;
                    box-sizing: border-box;
                    z-index: 2;
                }
                .form {
                    display: flex;
                    flex-direction: column;
                    align-items: flex-start;
                    justify-content: center;
                }
                
                
                .signup {
                    margin-top: 20px;
                    color: #3E3CF6;
                    text-decoration: none;
                    cursor: pointer;
                    transition: all .1s;
                }
                .signup a {
                    color: #3E3CF6;
                    text-decoration: none;
                    cursor: pointer;
                    transition: all .1s;
                }
                .signup:hover {
                    opacity: .6;
                }
                
                
                `}</style>
            </div>
        )
    }
}

class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showNext : false
        }
    }
    componentDidMount() {
        this.check(this.input.value)
    }
    handle = (e) => {
        this.props.handle(e)  
        this.check(e.target.value)
    }
    check = (value) => {
        if (this.props.check(value)) {
            this.setState({
                showNext : true
            })
        } else {
            this.setState({
                showNext : false
            })
        }
    }
    render() {
        const props = this.props
        const state = this.state
        return(
            <div className='signUpItem'>
                <div className='input'>
                    <label className='title'>{props.title}</label>
                    <input 
                        type={props.type}
                        value={props.value} 
                        ref={input => this.input = input}
                        onChange={e => this.handle(e)} 
                        required
                        placeholder={props.placeholder}
                    />
                    {props.showPass && <label 
                        className='showPass'>
                        <input className='check' type='checkbox' checked={props.showPassVal} onChange={(e) => props.showPass(e)}/>
                            Show Password
                    </label>}
                    <button 
                        type='button'
                        className={state.showNext ? 'button show' : 'button'} 
                        onClick={() => props.next()}
                    >Next</button>
                    {props.back &&<p 
                        type='button'
                        className='back'
                        onClick={() => props.back()}
                    >Back</p>}
                </div>
                <style jsx>{`
                    .input {
                        display: flex;
                        flex-direction: column;
                        align-items: flex-start;
                        justify-content: center;
                        text-align: center;
                        
                    }
                    .title {
                        font-size: 18px;
                    }
                    input {
                        width: 300px;
                        background-color: #F1F1F5;
                        min-height: 40px;
                        border: none;
                        font-family: 'objektiv-mk2', sans-serif;
                        font-size: 18px;
                        overlay: none;
                        outline: none;
                        padding: 0 10px;
                        box-sizing: border-box;
                        margin-bottom: 10px;
                        transition: all .1s;
                        border-radius: 10px;
                        margin-top: 3px;
                    }
                    input:focus {
                        
                    }
                    ::placeholder {
                        color: #C9C7D7;
                        font-weight: 300;
                    }
                    .button {
                        width: 300px;
                        height: ${state.showNext ? '50px' : '0'};
                        overflow: hidden;
                        border-radius: 25px;
                        border: none;
                        background-color: #3E3CF6;
                        color: white;
                        font-size: ${state.showNext ? '20px' : '0px' };
                        font-family: 'objektiv-mk2', sans-serif;
                        box-shadow: 0px 8px 25px 1px rgba(0,0,0,0.18);
                        transition: all .1s;
                        cursor: pointer;
                        outline: none;
                        padding: 0;
                        margin-top: 20px;
                        transform: scale(${state.showNext ? '1' : '0'});
                    }
                    button:hover {
                        box-shadow: 0px 4px 8px 1px rgba(0,0,0,0.22);
                        
                    }
                    button:active {
                        box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.26);
                        transform: scale(.990);
                    }
                    .check {
                        width: auto !important;
                        height: auto !important;
                        margin: 0;
                        margin-right: 5px;
                        margin-top: -2px;
                    }
                    .back {
                        color: #767485;
                        cursor: pointer;
                    }
                    .back:hover {
                        opacity: .6;
                    }
                    .showPass {
                        font-size: 12px;
                        font-weight: 300;
                        display: flex;
                        align-items: center;
                        justify-content: flex-start;
                        text-align: left;
                        margin-top: 5px;
                    }
                `}</style>
            </div>
        )
    }
}