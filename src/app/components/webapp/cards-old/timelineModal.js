import React, { Component } from 'react'

export default (props) => {
    return(
        <Modal>
            
        </Modal>
    )
}

const Modal = (props) => {
    return(
        <div className='modalContainer'>
            <div className='modalBg'>

            </div>
            <div className='modal'>

            </div>
            
            {props.children}
            <style jsx>{`
                .modal {
                    width: 450px;
                    height: 220px;
                    background-color: red;
                    position: absolute;
                    left: 50%;
                    top: 50%;
                    transform: translate(-50%, -50%);
                }
                .modalContainer {
                    position: fixed;
                    z-index: 50;
                    width: 100vw;
                    height: 100vh;
                    left: 0;
                    top: 0;
                    background-color: grey;
                }
                
            `}</style>
        </div>
    )
}