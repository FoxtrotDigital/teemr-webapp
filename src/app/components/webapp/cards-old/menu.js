
export default (props) => {
    let menuMapped = props.data.map((option, index) => 
        <div className='menuOption' onClick={() => {option.action(); option.toggleClose ? option.close() : null}}>
            <span className='menuIcon'>
                {option.icon}
            </span>
            {option.title}
            <style jsx>{`
                .menuOption {
                    height: 50px;
                    width: 100%;
                    display: flex;
                    flex-direction: row;
                    align-items: center;
                    justify-content: center;
                    cursor: pointer;
                    ${option.danger ? `color: #DB6565;` : null}
                    
                }
                .menuOption:hover {
                    background-color: #E5E6EC;
                }
                .menuIcon {
                    margin-right: 5px;
                    transform: translateY(2px);
                }
            `}</style>
        </div>
    )
    return(
        <div className={props.state ? 'menuArea menuOpen' : 'menuArea'}>
        {menuMapped}
        <style jsx>{`
            .menuArea {
                width: 100%;
                height: auto;
                max-height: 0px;
                margin-top: -1px;
                transition: opacity .2s, max-height .4s, margin-top .2s;
                background-color: #F1F1F5;
                color: #767485;
                font-size: 16px;
                font-weight: 300;
                overflow:hidden;
                position: relative;
            }
            .menuOpen {
                max-height: 100vh;
                height: auto;
                transition: opacity .2s, max-height .8s, margin-top .2s;
                
            }
            
        `}</style> 
        </div>
    )
}


export const TaskMenu = (props) => {
    let menuMapped = props.data.map((option, index) => 
        <div key={index} className='menuOption' onClick={() => {option.action(); option.toggleClose ? option.close() : null}}>
            <span className='menuIcon'>
                {option.icon}
            </span>
            {option.title}
            <style jsx>{`
                .menuOption {
                    height: 50px;
                    width: 100%;
                    display: flex;
                    flex-direction: row;
                    align-items: center;
                    justify-content: center;
                    cursor: pointer;
                    ${option.danger ? `color: #DB6565;` : null}
                    
                }
                .menuOption:hover {
                    background-color: #E5E6EC;
                }
                .menuIcon {
                    margin-right: 5px;
                    transform: translateY(2px);
                }
            `}</style>
        </div>
    )
    return(
        <div className={props.state ? 'menuArea menuOpen' : 'menuArea'}>
        {menuMapped}
        <style jsx>{`
            .menuArea {
                width: 100%;
                height: auto;
                max-height: 0px;
                margin-top: -1px;
                transition: opacity .2s, max-height .4s, margin-top .2s;
                background-color: #F1F1F5;
                color: #767485;
                font-size: 16px;
                font-weight: 300;
                overflow:hidden;
            
            }
            .menuOpen {
                max-height: 100vh;
                height: auto;
                transition: opacity .2s, max-height .8s, margin-top .2s;
                
            }
            
        `}</style> 
        </div>
    )
}

export const ColorPicker = (props) => {
    return(
        <div className='colorPicker' >
            <div 
                className='colorChoice'
                onClick={() => props.setColor('#DB6565')}
                style={{'background-color': '#DB6565'}}>
            </div>
            <div 
                className='colorChoice'
                onClick={() => props.setColor('#E0AC59')}
                style={{'background-color': '#E0AC59'}}>
            </div>
            <div 
                className='colorChoice'
                onClick={() => props.setColor('#A3C67B')}
                style={{'background-color': '#A3C67B'}}>
            </div>
            <div 
                className='colorChoice'
                onClick={() => props.setColor('#6085DF')}
                style={{'background-color': '#6085DF'}}>
            </div>
            <div 
                className='colorChoice'
                onClick={() => props.setColor('#9878D9')}
                style={{'background-color': '#9878D9'}}>
            </div>
            <style jsx>
            {`
                .colorPicker {
                    width: 100%;
                    height: 50px;
                    background-color: #F1F1F5;
                    position: absolute;
                    top: 50px;
                    z-index: 3;
                    display: flex;
                    flex-direction: row;
                    align-items: center;
                    justify-content: center;
                }
                .colorChoice {
                    margin: 5px;
                    width: 25px;
                    height: 25px;
                    border-radius: 20px;
                    border: 4px solid white;
                    transition: all .1s;
                    cursor: pointer;
                }
                .colorChoice:hover {
                    transform: scale(1.1);
                }
            `}</style>
        </div>
    )
}