import React, { Component } from 'react'
import { Plus, ChevronRight, MoreVertical, Circle, XCircle, X, FilePlus, CheckCircle, Edit2, Save, Trash, Users, Clock, FileText, Tag} from 'react-feather'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import * as Editable from './editable'
import Menu, { TaskMenu } from './menu'

export default (props) => {
    return(
        <div>
            {props.data.type === 'SIMPLE' && 
            <Simple 
                data={props.data} 
                updateData={(newData) => {props.updateData(newData)}} 
                expand={() => props.expand()} 
                shrink={() => props.shrink()}
                delete={() => props.delete()}
            />
            }
            {props.data.type === 'DETAILED' && 
            <Detailed 
                data={props.data} 
                updateData={(newData) => {props.updateData(newData)}} 
                expand={() => props.expand()} 
                shrink={() => props.shrink()}
                delete={() => props.delete()}
            />
            }
        </div>
    )
}

class Detailed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expand : false,
            newMenu : false,
            editMode : false,
            title : this.props.data.title,
            description : this.props.data.description,
            menu : false,
            reload : false
        }
    }
    componentWillReceiveProps(newProps) {
        this.setState({
            title : newProps.data.title,
            description : newProps.data.description,
            reload: true
        }, () => {
            this.setState({
                reload: false
            })
        })
    }
    toggleExpand = () => {
        this.setState(prevState => ({
            expand: !prevState.expand
        }), () => {
            if (this.state.expand) {
                this.props.expand();
            } else {
                this.props.shrink();
            }
        })
    }
    toggleNewMenu = () => {
        this.setState(prevState => ({
            newMenu: !prevState.newMenu
        }))
    }
    toggleMenu = () => {
        this.setState(prevState => ({
            menu: !prevState.menu
        }))
    }
    startEdit = () => {
        this.setState(prevState => ({
            editMode : true,
            prevTitle: prevState.title,
            prevDesc: prevState.description
        }))
    }
    titleEdit = (val) => {
        this.setState({
            title: val
        });
    }
    descEdit = (val) => {
        this.setState({
            description: val
        });
    }
    saveEdits = () => {
        this.setState({
            editMode : false
        }, () => {
            let data = this.props.data
            data.title = this.state.title
            data.description = this.state.description
            this.props.updateData(data)
        })
    }
    trashEdits = () => {
        this.setState(prevState => ({
            editMode : false,
            title: prevState.prevTitle,
            description: prevState.prevDesc
        }), () => {
        })
    }
    render() {
        
        return(
            <div className={this.state.expand ? 'task taskExpand' : 'task'}>
                <div 
                    className='taskHeaderBox' 
                    onClick={this.state.editMode ? null : () => this.toggleExpand()}
                >
                    <div className='taskTitle'>
                        <span className={this.state.expand ? 'taskIcon taskIconExpand' : 'taskIcon'}>
                            <ChevronRight size={18}/>
                        </span>
                        {!this.state.reload && 
                        <Editable.TaskTitle 
                        editMode={this.state.editMode}
                        value={this.state.title}
                        ref={(title) => this.title = title}
                        change={(val) => this.titleEdit(val)}
                        exitEdit={() => this.saveEdits()}
                        />}  
                    </div>
                    <div className='taskHeaderBg'>
                    </div>
                    
                </div>
                <div className={this.state.expand ? 'taskHeaderButtons taskHeaderButtonsExpand' : 'taskHeaderButtons'}>
                    <div className='taskHeaderButton' onClick={() => this.startEdit()}>
                        <Edit2 size={18}/>
                    </div>
                    <div className='taskHeaderButton' onClick={() => this.toggleMenu()}>
                        <MoreVertical size={18}/>
                    </div>
                </div>
                <div className={this.state.expand ? 'taskContent taskContentExpand' : 'taskContent'}>
                <TaskMenu 
                    state={this.state.menu}
                    data={[
                        {
                            title: 'Edit',
                            icon: <Edit2 size={18}/>,
                            action: () => this.startEdit(),
                            toggleClose : true,
                            close : () => this.toggleMenu()
                        },
                        {
                            title: 'Label Color',
                            icon: <Tag size={18}/>,
                            action: () => this.toggleColorPicker()
                        },
                        {
                            title: 'Timeline',
                            icon: <Clock size={18}/>,
                            action: () => null,
                            toggleClose : true,
                            close : () => this.toggleMenu()
                        },
                        {
                            title: 'Members',
                            icon: <Users size={18}/>
                        },
                        {
                            title: 'Delete',
                            icon: <Trash size={18}/>,
                            danger : true,
                            action: () => this.props.delete()
                        }
                    ]}
                />
                    <div className='taskDesc'>
                        <Editable.TaskDesc value={this.state.description} ref={(desc) => this.description = desc} change={(e) => this.descEdit(e)} />
                    </div>
                    
                    
                    <div className='taskAddSub' onClick={() => this.toggleNewMenu()}>
                    {this.state.newMenu 
                    ? <X /> 
                    
                    : <span className='taskAddSubTitle'>
                            <span className='taskAddSubIcon'>
                                <Plus size={18} />
                            </span>
                            New
                        </span>}
                    </div>
                    <div className={this.state.newMenu ? 'newMenu newMenuOpen' : 'newMenu'}>
                            <div className='newMenuItem' onClick={() => this.addCheckListItem()}>
                                <span className='taskAddSubIcon'>
                                    <CheckCircle size={20} />
                                </span>
                                Checklist Item
                            </div>
                            <div className='newMenuItem'>
                                <span className='taskAddSubIcon'>
                                    <FilePlus size={20} />
                                </span>
                                File
                            </div>
                    </div>
                </div>
                <style jsx> {`
                    .task {
                        min-height: 50px;
                        width: 100%;
                        position: relative;
                        transition: all .1s;
                        background-color: #F1F1F5;
                    }
                    .taskExpand {
                        z-index: 5;
                        box-shadow: 0px 3px 12px 1px rgba(0,0,0,0.18);
                    }
                    .taskHeaderBox {
                        width: calc(100% - 20px);
                        max-width: 420px;
                        min-height: 32px;
                        display: flex;
                        flex-direction: row;
                        justify-content: space-between;
                        align-items: center;
                        padding: 9px 10px;
                        box-sizing: content-box;
                        transition: all .1s;
                        ${this.state.editMode ? `cursor: default;` : `cursor: pointer;`}
                        position: relative;
                        z-index: 2;
                    }
                    .taskHeaderBox:hover .taskHeaderBg{
                        ${this.state.editMode ? null : `background-color: #E5E6EC;`}
                    }
                    .taskHeaderBg {
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 450px;
                        height: 100%;
                        z-index: 1
                        transition: all .1s;
                    }
                    .completionPerc {
                        font-size: 10px;
                        position: absolute;
                        right: 13px;
                        top: 18px;
                        color: #C9C7D7;
                        font-weight: 300;
                    }
                    .taskHeaderBox:hover .completionPerc {
                        color: #767485;
                    }
                    .taskHeaderButtons {
                        display: flex;
                        flex-direction: row;
                        align-items: center;
                        justify-content: flex-end;
                        position: absolute;
                        top: 0;
                        right: 0;
                        z-index: 0;
                        opacity: 0;
                        transition: all .1s;
                        height: 50px;
                        width: 100px;
                        margin: 0;
                    }
                    .taskHeaderButtonsExpand {
                        opacity: 1;
                        z-index: 2;
                        
                    }
                    .taskHeaderButton {
                        transition: all .1s;
                        height: 50px;
                        width: 50px;
                        margin: 0;
                        padding: 0;
                        color: #C9C7D7;
                        display: flex;
                        flex-direction: row;
                        align-items: center;
                        justify-content: center;
                        cursor: pointer;
                    }
                    .taskHeaderButton:hover {
                        background-color: #E5E6EC;
                        color: #767485;
                    }
                    .taskContent {
                        height: auto;
                        max-height: 0;
                        width: 450px;
                        transition: width .2s, opacity .2s, max-height .4s;
                        overflow: hidden;
                        opacity: 0;
                    }
                    .taskContentExpand {
                        transition: max-height 1.5s;
                        max-height: 1000vh;
                        opacity: 1;
                    }
                    .taskDesc {
                        color: #767485;
                        font-size: 12px;
                        font-weight: 300;
                        padding: 0 15px 0 35px;
                        border-bottom: 1px solid #E5E6EC;
                        padding-bottom: 15px;
                    }
                    .taskAddSub {
                        width: 100%;
                        height: 50px;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        font-size: 16px;
                        color: #C9C7D7;
                        font-weight: 300;
                        transition: all .1s;
                        cursor: pointer;
                    }
                    .taskAddSubIcon {
                        transform: translateY(3px);
                        margin-right: 4px;
                    }
                    .taskAddSubTitle {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }
                    .taskAddSub:hover {
                        background-color: #E5E6EC;
                        color: #767485;
                    }
                    .newMenu {
                        display: flex;
                        flex-direction: column;
                        align-items: center;
                        justify-content: flex-start;
                        width: 100%;
                        height: auto;
                        transition: all .1s;
                        max-height: 0;
                        overflow: hidden;
                    }
                    .newMenuOpen {
                        max-height: 100px;
                    }
                    .newMenuItem {
                        width: 100%;
                        height: 50px;
                        font-size: 16px;
                        color: #C9C7D7;
                        font-weight: 300;
                        transition: all .1s;
                        cursor: pointer;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }
                    .newMenuItem:hover {
                        background-color: #E5E6EC;
                        color: #767485;
                    }
                    .taskTitle {
                        display: flex;
                        flex-direction: row;
                        align-items: center;
                        justify-content: flex-start;
                        font-size: 17px;
                        font-weight: 300;
                        z-index: 2;
                    }
                    .saveIcon {
                        height: 50%;
                        width: 50px;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        color: #C9C7D7;
                        transition: all .1s;
                    }
                    .saveIcon:hover {
                        background-color: #E5E6EC;
                                color: #767485;
                    }
                    .trashIcon {
                        height: 50%;
                        width: 50px;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        color: #C9C7D7;
                        transition: all .1s;
                    }
                    .trashIcon:hover {
                        background-color: #E5E6EC;
                                color: #767485;
                    }
                    .editIcons {
                        position: absolute;
                        right: 0;
                        top: 0;
                        height: 100px;
                        width: 50px;
                        display: flex;
                        flex-direction: column;
                        align-items: center;
                        justify-content: space-around;
                        z-index: 2;
                        cursor: pointer;
                    }
                    .editIcon:hover {
                        background-color: #E5E6EC;
                                color: #767485;
                    }
                    .taskIcon {
                        transform: translateY(2px);
                        margin-right: 5px;
                        transition: all .2s;
                    }
                    .taskIconExpand {
                        transform: translateY(1px) translateX(-3px) rotateZ(90deg);
                        margin-right: 5px;
                    }
                `}</style>
            </div>
        )
    }
}

class Simple extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editMode: false,
            title: this.props.data.title,
            menu: false,
            reload: false,
            completion: this.props.completion
        }
    }
    componentWillReceiveProps(newProps) {
        this.setState({
            title : newProps.data.title,
            description : newProps.data.description,
            completion: newProps.data.completion,
            reload: true
        }, () => {
            this.setState({
                reload: false
            })
        })
    }
    startEdit = () => {
        this.setState(prevState => ({
            editMode : true,
            prevTitle: prevState.title
        }))
    }
    titleEdit = (val) => {
        this.setState({
            title: val
        });
    }
    saveEdits = () => {
        this.setState({
            editMode : false
        }, () => {
            let data = this.props.data
            data.title = this.state.title
            this.props.updateData(data)
        })
    }
    trashEdits = () => {
        this.setState(prevState => ({
            editMode : false,
            title: prevState.prevTitle
        }), () => {
        })
    }
    toggle = () => {
        this.setState(prevState => ({
            completion: prevState.completion === 1 ? 0 : 1
        }), () => {
            let data = this.props.data
            this.props.updateData(Object.assign({}, data, {
                completion: this.state.completion
            }))
        })
    }
    menuToggle = () => {
        this.setState(prevState => ({
            menu : !prevState.menu
        }))
    }
    render() {
        let props = this.props
        let state = this.state
        return(
            <div className='checkListItem' >
                <div className='checkListHitbox' onClick={this.state.editMode ? null : () => this.toggle()}>
                    <span className='checkListIcon'>
                        {this.state.completion === 1 ? <XCircle size={20}/> : <Circle size={20}/>}
                    </span>
                    <span className='checkListTitle'>
                        {!state.reload && <Editable.ChecklistTitle 
                            editMode={this.state.editMode}
                            value={this.state.title}
                            ref={(title) => this.title = title}
                            change={(val) => this.titleEdit(val)}
                            exitEdit={() => this.saveEdits()}
                            />}
                    </span>
                </div>
                
                <span className='preEditIcons'>
                    <span className='editIcon' onClick={() => this.startEdit()}>
                        <Edit2 size={18}/>
                    </span> 
                    <span className='editIcon' onClick={() => this.menuToggle()}>
                        <MoreVertical size={18}/>
                    </span> 
                    </span>
                
                <Menu 
                    state={this.state.menu}
                    data={[
                        {
                            title: 'Convert to Task',
                            icon: <FileText size={18}/>
                        },
                        {
                            title: 'Timeline',
                            icon: <Clock size={18}/>
                        },
                        {
                            title: 'Members',
                            icon: <Users size={18}/>
                        },
                        {
                            title: 'Delete',
                            icon: <Trash size={18}/>,
                            danger : true,
                            action: () => props.delete()
                        }
                    ]}
                />
                <style jsx>{`
                .checkListItem {
                    min-height: 50px;
                    width: 100%;
                    color: #767485;
                    font-weight: 300;
                    transition: all .1s;
                    ${this.state.editMode ? `cursor: default;` : `cursor: pointer;`}
                    position: relative;
                    background-color: #F1F1F5;
                }
                .checkListHitbox {
                    width: calc(100% - 50px);
                    transition: all .1s;
                    display: flex;
                    flex-direction: row;
                    justify-content: flex-start;
                    align-items: center;
                    min-height: 36px;
                    box-sizing: border-box;
                    padding: 7px 0px 7px ${this.props.child ? '25px' : '5px'};
                    
                }
                .checkListHitbox:hover {
                    ${this.state.editMode ? null : `background-color: #E5E6EC;`}
                }
                .checkListTitle {
                    max-width: calc(100% - 50px);
                    word-wrap: break-word;
                }
                .titleEdit {
                    z-index: 20;
                    height: 40px;
                }
                .checkListIcon {
                    margin-right: 5px;
                    height: 36px;
                    width: 35px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    transition: all .1s;
                }
                .checkListHitbox:hover .checkListIcon {
                    ${this.state.editMode ? null : `transform: scale(1.15);`}
                }
                .preEditIcons {
                    width: 60px;
                    height: 50px;
                    display: flex;
                    align-items: center;
                    justify-content: flex-end;
                    position: absolute;
                    right: 0;
                    top: 0;
                }
                .editIcon {
                    height: 100%;
                    width: 30px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    color: #C9C7D7;
                    transition: all .1s;
                }
                .saveIcon {
                    height: 50%;
                    width: 50px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    color: #C9C7D7;
                    transition: all .1s;
                }
                .saveIcon:hover {
                    background-color: #E5E6EC;
                            color: #767485;
                }
                .trashIcon {
                    height: 50%;
                    width: 50px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    color: #C9C7D7;
                    transition: all .1s;
                }
                .trashIcon:hover {
                    background-color: #E5E6EC;
                            color: #767485;
                }
                .editIcons {
                    position: absolute;
                    right: 0;
                    top: 0;
                    height: 100%;
                    width: 50px;
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    justify-content: space-around;
                    cursor: pointer;
                }
                .editIcon:hover {
                    background-color: #E5E6EC;
                            color: #767485;
                }
                `}</style>
            </div>
        )
    }
}