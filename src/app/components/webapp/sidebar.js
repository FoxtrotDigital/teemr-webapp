import React, { Component } from 'react'
import { Plus, Users, FileText, Settings, GitCommit, Calendar, HardDrive, Cloud  } from 'react-feather'
import Link from 'next/link'

export default class SideBar extends Component {
    render() {
        return(
            <div className='sidebar'>
                <NewProject newProject={() => this.props.newProject()}/>
                <div className='linkArea'>
                    <label className='linkAreaTitle'>Projects</label>
                        <a className='sidebarLink' onClick={() => this.props.toPage('CARDS')}> 
                            <FileText size={22}/>
                            <span className='linkText'>
                                Cards
                            </span>
                        </a>
                        <a className='sidebarLink' onClick={() => this.props.toPage('TIMELINE')} >
                            <GitCommit size={22}/>
                            <span className='linkText'>
                                Timeline
                            </span>
                        </a>
                        <a className='sidebarLink' onClick={() => this.props.toPage('CALENDAR')} >
                            <Calendar size={22}/>
                            <span className='linkText'>
                                Calendar
                            </span>
                        </a>
                    <span className='divider'></span>
                    <label className='linkAreaTitle'>Cloud</label>
                    <Link href='/webapp/files'>
                        <a className='sidebarLink'>
                            <HardDrive size={22}/>
                            <span className='linkText'>
                                My Files
                            </span>
                        </a>
                    </Link>
                    <Link href='/webapp/files'>
                        <a className='sidebarLink'>
                            <Cloud size={22}/>
                            <span className='linkText'>
                                Shared Files
                            </span>
                        </a>
                    </Link>
                    <span className='divider'></span>
                    <label className='linkAreaTitle'>People</label>
                    <Link href='/webapp/teams'>
                        <a className='sidebarLink'>
                            <Users size={22}/>
                            <span className='linkText'>
                                Teams
                            </span>
                        </a>
                    </Link>
                </div>
                <style jsx> {`
                    .sidebar {
                        height: 100%;
                        width: 220px;
                        position: absolute;
                        left: 0;
                        top: 0;
                        padding: 20px;
                        box-sizing: border-box;
                    }
                    .linkArea {
                        display: flex;
                        flex-direction: column;
                        align-items: flex-start;
                        justify-content: flex-start;
                        margin-top: 20px;
                        margin-left: 10px;
                    }
                    .linkAreaTitle {
                        font-size: 13px;
                        color: #C9C7D7;
                        font-weight: 300;
                        margin-bottom: 5px;
                    }
                    .sidebarLink {
                        margin-bottom: 15px;
                        text-decoration: none;
                        font-size: 18px;
                        color: #454354;
                        display: flex;
                        flex-direction: row;
                        align-items: center;
                        justify-content: flex-start;
                        transition: all .15s;
                    }
                    .sidebarLink:hover {
                        opacity: .6;

                    }
                    .divider {
                        width: 170px;
                        height: 0;
                        border-bottom: 1px solid #E5E6EC;
                        margin-bottom: 5px;
                    }
                    .linkText {
                        margin-left: 12px;
                    }
                    .icon {
                        margin-right: 20px;
                    }
                `} </style>
            </div>
        )
    }
}



const NewProject = (props) => {
    return(
        <button className='newProject' onClick={props.newProject}>
            <Plus size={25}/>
                New Project
            <style jsx> {`
            .newProject {
                width: 180px;
                height: 50px;
                border: none;
                border-radius: 10px;
                font-size: 19px;
                font-family: 'objektiv-mk2', sans-serif;
                font-weight: 400;
                display: flex;
                flex-direction: row;
                align-items: center;
                background-color: white;
                justify-content: space-between;
                padding: 0 18px 0 12px;
                -webkit-box-shadow: 0px 6px 25px 1px rgba(0,0,0,0.16);
                -moz-box-shadow: 0px 6px 25px 1px rgba(0,0,0,0.16);
                box-shadow: 0px 6px 25px 1px rgba(0,0,0,0.16);
                transition: all .1s;
                cursor: pointer;
                color: #454354;
                outline: none;
            }
            .newProject:hover {
                -webkit-box-shadow: 0px 2px 4px 1px rgba(0,0,0,0.20);
                -moz-box-shadow: 0px 2px 4px 1px rgba(0,0,0,0.20);
                box-shadow: 0px 2px 4px 1px rgba(0,0,0,0.20);
                transform: scale(.995)
            }
            .newProject:active {
                -webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.24);
                -moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.24);
                box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.24);
                transform: scale(.98)
            }
                    
                `} </style>
        </button>
    )
}

const LangChanger = (props) => {
    return(
        <div className='langChanger'>
            <button className='lang' onClick={props.toEN}>
                EN
            </button>
            <div  className='line'>
                |
            </div>
            <button className='lang' onClick={props.toTH}>
                TH
            </button>
            <style jsx>{`
            .langChanger {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: flex-start;
                position: absolute;
                bottom: 15px;
                left: 15px;
            }
            .line {
                display: inline-block;
            }
            .lang {
                text-decoration: none;
                background: transparent;
                border: none;
                font-size: 14px;
                margin: 0 3px;
                padding: 0;
                display: inline-block;
                font-family: 'objektiv-mk2', sans-serif;
            }
            `}</style>
        </div>
    )
}