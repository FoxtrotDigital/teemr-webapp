import React, { Component } from 'react'
import WebApp from './webapp-new'

export default (props) => {
    return(
            <div>
                <WebApp />
                <style jsx>{`
                    :global(body) {
                        padding: 0;
                        margin: 0;
                        background-color: #F1F1F5;
                        font-family: 'objektiv-mk2', sans-serif;
                        color: #3D3A51;
                        overflow: hidden;
                        box-sizing: border-box;
                        -webkit-font-smoothing: antialiased;
                        text-rendering: optimizeLegibility; 
                    }
                    
                `}</style>
            </div>
    )
  
}