import Link from 'next/link'
import Header from '../components/header'
import Head from '../components/head-public'
export default () => (
  <div>
  <Head />
  <Header />

    <style jsx>{`
      :global(body) {
        box-sizing: border-box;
        padding: 0;
        margin: 0;
        background-color: #F1F1F5;
        font-family: 'objektiv-mk2', sans-serif;
      }
      
    `}</style>
  </div>
)
