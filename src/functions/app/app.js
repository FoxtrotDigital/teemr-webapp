//Import Firebase apis
import * as functions from "firebase-functions"
import * as admin from 'firebase-admin'

//Import Express
import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
//Import Nextjs
import next from "next"

//Initialize Firebase apis
admin.initializeApp(functions.config().firebase);
//Initialize Cloud Firestore
var db = admin.firestore();

//Initialize Nextjs handler
const dev = process.env.NODE_ENV !== "production"
const app = next({ dev, conf: { distDir: "next" } })
const handle = app.getRequestHandler()

//Parepare Nextjs
app.prepare()



//AUTH
var auth = express.Router()

auth.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

// define the about route
auth.post('/login', function (req, res) {
    
})

auth.post('/signup', (req, res) => {
    let user = db.collection('users').doc(req.body.uid)
    let setUser = user.set({
        uid : req.body.uid,
        name : {
            first : req.body.first,
            last : req.body.last
        },
        email : req.body.email,
        phone : req.body.phone,
        profilePhotoLink : req.body.photoLink,
        type: 'NORMAL',
        notifications: [],
        projects: [],
        files: [],
        connections: [], //Friends and people they came in contact with
        teams: [], //Teams they're part of
        prefs: {}
      });
})






//API
var api = express.Router()

// middleware that is specific to this router
api.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})
api.use('/auth', auth)

api.get('*', function (req, res) {
  res.send('Invalid Request Dude.')
})





//Intialize Express
const exp = express();

  //Express Middleware
  exp.use(cors({ origin: true }))
  exp.use(bodyParser.json());

  //Redirects all /api requests to the api router
  exp.use('/api', api)

  //Everything else goes to Nextjs
  exp.get('*', (req, res) => {
      handle(req, res)
  })

export const nextApp = functions.https.onRequest(exp)