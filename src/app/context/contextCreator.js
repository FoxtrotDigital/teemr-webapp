//Context
import React, { Component, createContext } from 'react'

const context = {
    auth: {
        userData : null,
        loggedIn : false,
        result : null
    },
    loading : false
}
export const Context = createContext(
    context // default value
);