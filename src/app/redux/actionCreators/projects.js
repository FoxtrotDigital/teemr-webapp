import * as Actions from '../actions'

export default (db) => {
    const creators = {
        new : (userId) => {
            return dispatch => {
                dispatch({type: Actions.GLOBAL.LOADING})
                let projCol = db.collection('projects')
                projCol.get().then((projsSnap) => {
                    let projs = []
                    projsSnap.forEach((project) => {
                        projs.push(1)
                    })
                    let docRef = projCol.doc()
                    let newProject = {
                        id: docRef.id,
                        title: 'New Project',
                        labelColor: '#DB6565',
                        members : { [userId] : true },
                        cards: {},
                        rank: projs.length
                    }
                    docRef.set(newProject)
                    .then(function() {
                        dispatch({type: Actions.GLOBAL.DONELOADING})
                        console.log("Project successfully written!");
                    })
                    .catch(function(error) {
                        console.error("Error writing document: ", error);
                        dispatch({type: Actions.GLOBAL.DONELOADING})
                        return
                    });
                })
            }
            
        },
        delete : (pId) => {
            return dispatch => {
                dispatch({type: Actions.GLOBAL.LOADING})
                    db.collection("projects").doc(pId).delete().then(function() {
                        db.collection('tasks').where(`parent`, "==", pId)
                            .get()
                            .then((colRef) => {
                                colRef.forEach((docRef) => {
                                    db.collection('tasks').doc(docRef.id).delete()
                                })
                            }).then(() => {
                                console.log("Document successfully deleted!");
                                dispatch({type: Actions.GLOBAL.DONELOADING})
                                dispatch({type: Actions.PROJECTS.DELETE})
                            })
                        
                    }).catch(function(error) {
                        console.error("Error removing document: ", error);
                    });
                
            }
        },
        listen : (uid) => {
            let projects = {}
            let runs = 0
            return (dispatch) => {
                //Dispatch that something is being loaded
                dispatch({type : Actions.GLOBAL.LOADING})
                //Get a Query containing projects where the user is a member of and listen
                
                return db.collection("projects").where(`members.${uid}`, "==", true)
                .onSnapshot((querySnap) => {
                    //Iterate through every project
                    let updates = {}
                    for (let i = 0; i < querySnap.docChanges.length; i++) {
                        //Initialize the inital value of 0 updates
                        if (!updates[i]) {
                            updates[i] = 0
                        }
                        let doc = querySnap.docChanges[i].doc;
                        //Listen to every single doc
                        let docRef = db.collection("projects").doc(doc.id)
                        //Listen to each project's document
                        docRef.onSnapshot((docSnap) => {
                            dispatch({type: Actions.GLOBAL.LOADING})
                            if (updates[i] === 0) { //Initial 
                                //Add the doc to the projects object
                                projects[doc.id] = docSnap.data()
                            } else { //Everytime after
                                projects[doc.id] = Object.assign({}, projects[doc.id], docSnap.data())
                            }
                            //If it's undefined or null, delete it.
                            if (projects[doc.id] === null || projects[doc.id] === undefined) {
                                delete projects[doc.id]
                            }
                            //Document [i] has updated once
                            updates[i]++
                            if (runs >= 1) {
                                dispatch(creators.update(projects))
                                runs++
                                dispatch({type: Actions.GLOBAL.DONELOADING})
                            }
                        })
                    }
                    dispatch(creators.update(projects))
                    runs++            
                    dispatch({type: Actions.GLOBAL.DONELOADING})
                });
            }
        },
        update : (projectsData) => {
            return {
                type: Actions.PROJECTS.UPDATE,
                data : projectsData,
                updateTime: Date.now()
            }
        },
        updateTitle : (pId, title) => {
            return dispatch => {
                dispatch({type : Actions.GLOBAL.LOADING})
                let docRef = db.collection('projects').doc(pId)
                docRef.update({
                    title: title
                }).then(() => {
                    dispatch({type : Actions.GLOBAL.DONELOADING})
                }).catch((err) => {
                    console.log(err)
                })
            }
        },
        updateLabelColor : (pId, color) => {
            return dispatch => {
                dispatch({type : Actions.GLOBAL.LOADING})
                let docRef = db.collection('projects').doc(pId)
                docRef.update({
                    labelColor: color
                }).then(() => {
                    dispatch({type : Actions.GLOBAL.DONELOADING})
                }).catch((err) => {
                    console.log(err)
                })
            }
        }
        //TODO: Add Members
        //TODO: Dates & Timeline
    }
    return creators

    
}