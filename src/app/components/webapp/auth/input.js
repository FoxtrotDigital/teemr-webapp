import React, { Component } from 'react'

//TODO: Type checking and default props.

export default class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showNext : false
        }
    }
    componentDidMount() {
        this.check(this.input.value)
    }
    handle = (e) => {
        this.props.handle(e)  
        this.check(e.target.value)
    }
    handleEnter = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault()
            if (this.state.showNext) {
                if (this.props.submit) {
                    this.props.submit()
                } else {
                    this.props.next()
                }
            }
        }
    }
    check = (value) => {
        if (this.props.check(value)) {
            this.setState({
                showNext : true
            })
        } else {
            this.setState({
                showNext : false
            })
        }
    }
    render() {
        const props = this.props
        const state = this.state
        return(
            <div className='signUpItem'>
                <div className='input'>
                    <label className='title'>{props.title}</label>
                    <input 
                        type={props.type}
                        value={props.value} 
                        ref={input => this.input = input}
                        onChange={e => this.handle(e)} 
                        onKeyPress={e => this.handleEnter(e)}
                        placeholder={props.placeholder}
                    />
                    {props.showPass && <label 
                        className='showPass'>
                        <input className='check' type='checkbox' checked={props.showPassVal} onChange={(e) => props.showPass(e)}/>
                            Show Password
                    </label>}
                    {props.next && <button 
                        type='button'
                        className={state.showNext ? 'button show' : 'button'} 
                        onClick={() => props.next()}
                    >Next</button>}
                    {props.submit && <button 
                        type='button'
                        className={state.showNext ? 'button show' : 'button'} 
                        onClick={() => props.submit()}
                    >{props.submitText}</button>
                    }
                    {props.back &&<p 
                        type='button'
                        className='back'
                        onClick={() => props.back()}
                    >Back</p>}
                </div>
                <style jsx>{`
                    .input {
                        display: flex;
                        flex-direction: column;
                        align-items: center;
                        justify-content: center;
                        text-align: center;
                    }
                    .title {
                        font-size: 22px;
                        margin-bottom: 10px;
                    }
                    input {
                        width: 300px;
                        background-color: #F1F1F5;
                        height: 50px;
                        border: none;
                        border-radius: 8px;
                        font-family: 'objektiv-mk2', sans-serif;
                        font-size: 18px;
                        overlay: none;
                        outline: none;
                        padding: 0 10px;
                        box-sizing: border-box;
                        margin-bottom: 10px;
                        border: 1px solid #E5E6EC;
                        transition: all .2s;
                        text-align: center;
                    }
                    input:focus {
                        border: 2px solid rgba(62, 60, 246, 0.8);
                    }
                    ::placeholder {
                        color: #C9C7D7;
                        font-weight: 300;
                    }
                    .button {
                        width: 300px;
                        height: ${state.showNext ? '50px' : '0'};
                        overflow: hidden;
                        border-radius: 25px;
                        border: none;
                        background-color: #3E3CF6;
                        color: white;
                        font-size: ${state.showNext ? '20px' : '0px' };
                        font-family: 'objektiv-mk2', sans-serif;
                        box-shadow: 0px 8px 26px 1px rgba(62, 60, 246, 0.22);
                        transition: all .1s;
                        cursor: pointer;
                        outline: none;
                        padding: 0;
                        margin-top: 20px;
                        transform: scale(${state.showNext ? '1' : '0'});
                    }
                    button:hover {
                        box-shadow: 0px 4px 8px 1px rgba(62, 60, 246, 0.28);
                        
                    }
                    button:active {
                        box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.26);
                        transform: scale(.990);
                    }
                    .check {
                        width: auto !important;
                        height: auto !important;
                        margin: 0;
                        margin-right: 5px;
                        margin-top: -2px;
                    }
                    .back {
                        color: #767485;
                        cursor: pointer;
                    }
                    .back:hover {
                        opacity: .6;
                    }
                    .showPass {
                        font-size: 12px;
                        font-weight: 300;
                        display: flex;
                        align-items: center;
                        justify-content: flex-start;
                        text-align: left;
                        margin-top: 5px;
                    }
                `}</style>
            </div>
        )
    }
}