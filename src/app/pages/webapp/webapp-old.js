import React, { Component } from 'react'
import Router from 'next/router'
import { createStore, applyMiddleware } from 'redux'
import { Provider, connect } from 'react-redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { projects, auth }  from '../../redux/actionCreators'
import teemr from '../../redux/reducers'
import { BubbleLoader } from 'react-css-loaders';

//Import Components
import Header from '../../components/webapp/header'
import SideBar from '../../components/webapp/sidebar'
import Cards from '../../components/webapp/cards/cards'
import AuthDialog from '../../components/webapp/auth/authDialog'

//Import images
import Logo from './logo.svg'


const store = createStore(
    teemr,
    applyMiddleware(thunk)
)

const SplashScreen = (props) => {
    return(
        <div className='splashScreen'>
            <Logo style={{height: '100px', width: '80px'}}/>
            <style jsx>{`
                .splashScreen {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    z-index: 30;
                    position: fixed;
                    background-color: white;
                    width: 100vw;
                    height: 100vh;
                    transition: all .3s;
                    opacity: ${props.fadeOut ? '0' : '1'}
                }
            `}</style>
        </div>
    )
}

class WebApp extends Component {
    constructor(props) {
        super(props)
        this.auth()
        this.state = {
            loginDialog : this.props.loggedIn,
            splashDialog : true,
            fadeOut : false
        }
    }
    componentWillReceiveProps(newProps) {
        if (!newProps.loggedIn) {
            this.setState({
                loginDialog : true,
                fadeOut : true
            }, () => {
                setTimeout(() => {
                    this.setState({
                        splashDialog : false
                    })
                }, 310)
            })
        } else {
            this.setState({
                loginDialog : false,
                fadeOut : true
            }, () => {
                setTimeout(() => {
                    this.setState({
                        splashDialog : false
                    })
                }, 310)
            })
        }
    }
    auth = async () => {
        this.props.getAuthStatus()
    }
    render() {
        return(
            <div className='webapp'>
                <Header logout={() => this.props.logout()}/>
                {this.state.splashDialog &&
                    <SplashScreen fadeOut={this.state.fadeOut}/>
                }
                {this.state.loginDialog &&
                    <AuthDialog func={'LOGIN'} login={(email, password) => this.props.login(email, password)} result={this.props.result}/>
                }
                <div className='webapp-content'>
                    <SideBar newProject={() => this.props.newProject(this.props.userData.uid)}/>
                    {this.props.children}
                    <Cards />
                    {this.props.loading && <div className='loading'><BubbleLoader size={6} duration={1.2} color={'#C9C7D7'}/></div>}
                </div>
                <style jsx>{`
                    .webapp-content {
                        top: 50px;
                        position: absolute;
                        height: calc(100vh - 50px);
                        width: 100vw;
                        color: #3D3A51;
                        
                    }
                    .loading {
                        position: fixed;
                        bottom: 20px;
                        left: 15px;
                        height: 50px;
                        width: 80px;
                        margin: 0;
                        padding: 0;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }
                `}</style>
            </div>
            
        )
    }
}

const mapStateToProps = state => {
    return {   
      userData : state.auth.userData,
      result : state.auth.result,
      loggedIn : state.auth.loggedIn,
      loading: state.global.loading
    }
  }
   
const mapDispatchToProps = dispatch => {
    return {
      newProject : (userId) => {
          dispatch(projects.new(userId))
      },
      getAuthStatus: () => {
          dispatch(auth.getStatus())
        },
      listenToProjects: (userId) => {
           dispatch(projects.listen(userId))
      },
      login : (email, password) => {
          dispatch(auth.login(email, password))
      },
      logout : () => {
          dispatch(auth.logout())
      }
    }
  }
   
const ConnectedWebApp = connect(
    mapStateToProps,
    mapDispatchToProps
  )(WebApp)

export default () => {
    return(
        <Provider store={store}>
            <ConnectedWebApp />
        </Provider>
    )
};