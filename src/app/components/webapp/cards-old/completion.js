export default (props) => {
    return(
        <span className='completionCover'>
            <div className='completionBox'>
                <div className='completion'>
                </div>
            </div>
            {Math.round(props.completion * 100).toString().slice(0,3)}%
            <style jsx>{`
            .completionCover {
                font-size: 10px;
                width: 100%;
                display: flex;
                flex-direction: row;
                align-items: center;
                height: 100%;
                justify-content: flex-start;
                color: #767485;
                font-weight: 300;
                margin-left: 28px;
            }
            .completion {
                width: ${props.completion * 100}%;
                height: 3px;
                background-color: ${props.completion === 1 ? `#A3C67B;` : `#767485;`}
                border-radius: 1.5px;
                transition: all .2s;
            }
            .completionBox {
                height: 3px;
                margin-right: 5px;
                width: calc(100% - 50px);
                border-radius: 1.5px;
                box-sizing: border-box;
                background-color: #E5E6EC;
                display: flex;
                flex-direction: row;
                align-items:center;
                justify-content: flex-start;
                overflow: hidden;
            }
            `}</style>
        </span>
    )
}